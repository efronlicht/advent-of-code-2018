fn main() {
    let (initial_state, rules) = parse_input(INPUT);
    println!("{}", a(INPUT));
}

const INPUT: &str = include_str!("a.in");

use std::collections::VecDeque;

fn count_plants<'a>(it: impl IntoIterator<Item = &'a bool>) -> usize {
    it.into_iter().filter(|b| **b).count()
}

fn chain3<T>(
    a: impl IntoIterator<Item = T>,
    b: impl IntoIterator<Item = T>,
    c: impl IntoIterator<Item = T>,
) -> impl Iterator<Item = T> {
    a.into_iter().chain(b).chain(c)
}

fn twice<T: Clone>(t: T) -> impl Iterator<Item = T> {
    std::iter::repeat(t).take(2)
}

fn a(s: &str) -> usize {
    let (mut line, rules) = parse_input(s);
    let mut total = count_plants(&line);
    for generation in 0..20 {
        line = match (line.iter().position(|b| *b), line.iter().rposition(|b| *b)) {
            (Some(front), Some(back)) => chain3(
                twice(false),
                line.into_iter().skip(front).take(back - front + 1),
                twice(false),
            )
            .collect::<Vec<bool>>(),
            _ => panic!("should have had at least one live element"),
        };
        total += count_plants(&line);
        println!(
            "{}",
            line.iter()
                .skip_while(|b| !*b)
                .map(|b| as_char(*b))
                .collect::<String>()
        );

        let prev = line.clone();
        for i in 2..line.len() - 2 {
            let pattern = (prev[i - 2], prev[i - 1], prev[i], prev[i + 1], prev[i + 2]);
            for rule in &rules {
                if let Some(output) = rule.apply(pattern) {
                    line[i] = output;
                    break;
                }
            }
        }
    }
    0
}
const TEST_INPUT: &str = include_str!("test_input.in");

#[test]
fn test_a() {
    assert_eq!(a(TEST_INPUT), 325)
}
#[test]
fn test_parse_input() {
    let want_state: Vec<bool> = "#..#.#..##......###...###"
        .bytes()
        .map(|b| b == b'#')
        .collect();
    let mut want_rules = vec![
        (false, false, false, true, true),
        (false, false, true, false, false),
        (false, true, false, false, false),
        (false, true, false, true, false),
        (false, true, false, true, true),
        (false, true, true, false, false),
        (false, true, true, true, true),
        (true, false, true, false, true),
        (true, false, true, true, true),
        (true, true, false, true, false),
        (true, true, false, true, true),
        (true, true, true, false, false),
        (true, true, true, false, true),
        (true, true, true, true, false),
    ];
    want_rules.sort();
    let want_rules = want_rules
        .into_iter()
        .map(|pattern| Rule { pattern, out: true })
        .collect();

    assert_eq!((want_state, want_rules), parse_input(TEST_INPUT));
}
fn parse_input(s: &str) -> (Vec<bool>, Vec<Rule>) {
    let mut lines = s.lines();
    let initial_state = parse_initial_state(lines.next().unwrap());
    let mut rules: Vec<Rule> = lines.filter_map(Rule::parse).collect();
    rules.sort();
    (initial_state, rules)
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord)]
struct Rule {
    pattern: (bool, bool, bool, bool, bool),
    out: bool,
}

impl Rule {
    fn parse(s: &str) -> Option<Rule> {
        let p: Vec<bool> = s
            .bytes()
            .filter_map(|b| match b {
                b'#' => Some(true),
                b'.' => Some(false),
                _ => None,
            })
            .collect();
        if p.len() != 6 {
            None
        } else {
            let r = Rule {
                pattern: (p[0], p[1], p[2], p[3], p[4]),
                out: p[5],
            };
            println! {"{:?}", r};
            Some(r)
        }
    }

    fn apply(self, pattern: (bool, bool, bool, bool, bool)) -> Option<bool> {
        if self.pattern == pattern {
            Some(self.out)
        } else {
            None
        }
    }

    fn display(self) -> String {
        let (a, b, c, d, e) = self.pattern;
        format!(
            "{}{}{}{}{}=> {}",
            as_char(a),
            as_char(b),
            as_char(c),
            as_char(d),
            as_char(e),
            as_char(self.out),
        )
    }
}

fn as_char(b: bool) -> char {
    if b {
        '#'
    } else {
        '.'
    }
}

fn parse_initial_state(s: &str) -> Vec<bool> {
    (s.bytes().filter_map(|b| match b {
        b'#' => Some(true),
        b'.' => Some(false),
        _ => None,
    }))
    .collect()
}
const INIT_STATE: [u8;100] = *b"#.#.#..##.#....#.#.##..##.##..#..#...##....###..#......###.#..#.....#.###.#...#####.####...#####.#.#";
