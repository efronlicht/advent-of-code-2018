const INPUT: &str = include_str!("a.in");
extern crate regex;
#[macro_use]
extern crate lazy_static;
use regex::Regex;

fn main() {
    println!("aoc_2018_03_a => {}", a(INPUT));
    println!("aoc_2018_03_b => {}", b(INPUT));
}

fn a(input: &str) -> usize {
    let mut fabric: [[u8; 1000]; 1000] = [[0; 1000]; 1000];
    for Rectangle {
        left,
        top,
        bottom,
        right,
    } in input.lines().map(|s| s.parse::<Rectangle>().unwrap())
    {
        for i in left..right {
            for j in top..bottom {
                fabric[i as usize][j as usize] += 1;
            }
        }
    }
    fabric
        .iter()
        .flat_map(|a| a.iter())
        .filter(|n| **n > 1)
        .count()
}

fn b(input: &str) -> usize {
    let rectangles: Vec<Rectangle> = input.lines().map(|s| s.parse().unwrap()).collect();
    for (i, a) in rectangles.iter().enumerate() {
        if rectangles.iter().filter(|b| a.intersect(**b)).count() == 1 {
            return i + 1;
        }
    }
    unreachable!()
}
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct Rectangle {
    left: u16,
    right: u16,
    top: u16,
    bottom: u16,
}

impl Rectangle {
    fn intersect(self, other: Self) -> bool {
        !(self.right > other.left
            || other.right < self.left
            || self.top > other.bottom
            || other.top > self.bottom)
    }
}

impl std::str::FromStr for Rectangle {
    type Err = std::num::ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r".+@ (\d+),(\d+): (\d+)x(\d+)").unwrap();
        }
        match RE.captures(s) {
            Some(ref cap) if cap.len() == 5 => {
                let (left, top, width, height) = (
                    cap[1].parse()?,
                    cap[2].parse()?,
                    cap[3].parse::<u16>()?,
                    cap[4].parse::<u16>()?,
                );
                let (bottom, right) = (top + height, left + width);
                Ok(Rectangle {
                    left,
                    top,
                    bottom,
                    right,
                })
            }

            _ => Err(s.parse::<usize>().unwrap_err()),
        }
    }
}

#[test]
fn test_a() {
    let input = "#1 @ 1,3: 4x4
    #2 @ 3,1: 4x4
    #3 @ 5,5: 2x2";
    assert_eq!(a(input), 4);
}
#[test]
fn test_from_str() {
    assert_eq!(
        Rectangle {
            left: 483,
            top: 830,
            right: 483 + 24,
            bottom: 830 + 18,
        },
        "#1 @ 483,830: 24x18".parse().unwrap()
    )
}
