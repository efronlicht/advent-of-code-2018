fn main() {
    println!("aoc_2018_022_a => {}", a(TARGET, DEPTH));
}
use self::Region::*;
use self::Tool::*;
use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::hash::Hash;
use std::num::NonZeroUsize;

type AdjacencyList = HashMap<Node, Vec<Node>>;
type SmallVec<T> = smallvec::SmallVec<[T; 5]>;
struct Cache {
    map: HashMap<Point, usize>,
    depth: usize,
    target: Point,
}

impl Node {
    /// the complement node; same region and location, but different tool
    fn complement(self) -> Self {
        Node {
            pos: self.pos,
            state: self.state.complement(),
        }
    }
    fn neighbors(self, regions: &HashMap<Point, Region>, bounds: (u16, u16)) -> Vec<Self> {
        let Node { pos: (y, x), state } = self;
        let (max_y, max_x) = bounds;

        let mut neighboring_regions = SmallVec::new();
        if y > 0 {
            let p = (y - 1, x);
            neighboring_regions.push((p, regions[&p]));
        }
        if y < max_y {
            let p = (y + 1, x);
            neighboring_regions.push((p, regions[&p]));
        }
        if x > 0 {
            let p = (y, x - 1);
            neighboring_regions.push((p, regions[&p]));
        }
        if x < max_x {
            let p = (y, x - 1);
            neighboring_regions.push((p, regions[&p]));
        }

        let complement = std::iter::once(self.complement());
        let neighbors = neighboring_regions.iter().map(|(pos, r)| Node {
            pos: *pos,
            state: State::partner(state, *r).unwrap(),
        });

        complement.chain(neighbors).collect()
    }
}

impl Cache {
    fn build_neighbors(&self, bounds: Point) -> HashMap<Node, Vec<Node>> {
        let regions: HashMap<Point, Region> = self
            .map
            .iter()
            .map(|(k, v)| (*k, Region::from_erosion_level(*v)))
            .collect();

        let mut neighbors = HashMap::with_capacity(regions.len() * 2);

        for (pos, r) in regions.iter() {
            let (a, b) = State::from_region(*r);
            let pos = *pos;
            let (a, b) = (Node { pos, state: a }, Node { pos, state: b });
            neighbors.insert(a, a.neighbors(&regions, bounds));
            neighbors.insert(b, b.neighbors(&regions, bounds));
        }
        neighbors
    }
}
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Node {
    pos: Point,
    state: State,
}

fn dijekstra<N, F>(
    start: N,
    adjacency_list: impl IntoIterator<Item = (N, Vec<N>)>,
    mut transition_cost: F,
) -> HashMap<N, N>
where
    N: Hash + Eq + PartialOrd + Clone,
    F: FnMut(&N, &N) -> usize,
{
    let mut neighbors: HashMap<N, Vec<N>> = adjacency_list.into_iter().collect();
    let (mut prev, mut unvisited) = (
        HashMap::with_capacity(neighbors.len()),
        neighbors.keys().cloned().collect(),
    );
    let mut distances: HashMap<N, usize> = neighbors
        .keys()
        .cloned()
        .map(|k| (k, std::usize::MAX))
        .collect();
    distances.insert(start, 0);

    while !unvisited.is_empty() {
        let current = (*unvisited
            .iter()
            .min_by_key(|node| distances[*node])
            .unwrap())
        .clone();
        unvisited.remove(&current);
        let prev_dist: usize = match distances[&current] {
            std::usize::MAX => continue,
            n => n,
        };
        for neighbor in &neighbors[&current] {
            let cost = prev_dist + transition_cost(&current, neighbor);
            let v = distances.get_mut(neighbor).unwrap();
            if cost < *v || cost <= *v && *neighbor < current {
                prev.insert(neighbor.clone(), current.clone());
                *v = cost;
            }
        }
    }
    prev
}

impl Node {}

type Map = HashMap<Point, State>;

impl State {
    fn transition_cost(self, other: Self) -> Option<usize> {
        if self.1 == other.1 {
            Some(1)
        } else if self.0 == other.0 {
            Some(7)
        } else {
            None
        }
    }
}

type Point = (u16, u16);
const DEPTH: usize = 11109;
const TARGET_Y: u16 = 731;
const TARGET_X: u16 = 9;
const TARGET: (u16, u16) = (TARGET_Y, TARGET_X);

const EROSION_MODULUS: usize = 20183;
const GEO_X_SCALAR: usize = 16807;
const GEO_Y_SCALAR: usize = 48271;
const TEST_TARGET: (u16, u16) = (10, 10);
const TEST_DEPTH: usize = 510;
#[test]
fn test_a() {
    const DEPTH: usize = 510;
    const WANT: usize = 114;
    assert_eq!(a(TEST_TARGET, TEST_DEPTH), WANT);
}

fn a(target: Point, depth: usize) -> usize {
    let mut cache = Cache {
        map: HashMap::new(),
        depth,
        target,
    };
    (0..=target.0)
        .cartesian_product(0..=target.1)
        .map(|p| cache.erosion_level(p) % 3)
        .sum()
}
impl Cache {
    fn erosion_level(&mut self, p: Point) -> usize {
        if let Some(n) = self.map.get(&p) {
            *n
        } else {
            let geologic_index = match p {
                (0, 0) => 0,
                p if p == self.target => 0,
                (0, x) => x as usize * GEO_X_SCALAR,
                (y, 0) => y as usize * GEO_Y_SCALAR,
                (y, x) => {
                    let up = self.erosion_level((y - 1, x));
                    let right = self.erosion_level((y, x - 1));
                    up * right % EROSION_MODULUS
                }
            };
            let erosion_level = (geologic_index + self.depth) % EROSION_MODULUS;
            self.map.insert(p, erosion_level);
            erosion_level
        }
    }
}
#[test]
fn test_erosion_level() {
    let mut cache = Cache {
        map: HashMap::new(),
        depth: TEST_DEPTH,
        target: TEST_TARGET,
    };
    for (p, want) in vec![
        ((0, 0), 510),
        ((0, 1), 17317),
        ((1, 0), 8415),
        (TEST_TARGET, 510),
    ] {
        assert_eq!(cache.erosion_level(p), want);
    }
}
pub struct Regions(std::collections::BTreeMap<(u16, u16), Region>);

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct State(Region, Tool);

impl State {
    fn partner(self, r: Region) -> Option<Self> {
        let State(region, tool) = self;
        match (region, r, tool) {
            (a, b, _) if a == b => Some(self),
            (Rocky, Wet, Gear)
            | (Rocky, Narrow, Torch)
            | (Narrow, Rocky, Torch)
            | (Narrow, Wet, Neither)
            | (Wet, Rocky, Gear)
            | (Wet, Narrow, Neither) => Some(State(r, tool)),
            _ => None,
        }
    }

    fn complement(self) -> Self {
        let State(region, tool) = self;
        let tool = match (region, tool) {
            (Rocky, Gear) => Torch,
            (Rocky, Torch) => Gear,
            (Narrow, Torch) => Neither,
            (Narrow, Neither) => Torch,
            (Wet, Neither) => Gear,
            (Wet, Gear) => Neither,
            _ => unreachable!(),
        };
        State(region, tool)
    }

    fn from_region(r: Region) -> (Self, Self) {
        let (a, b) = match r {
            Rocky => (Gear, Torch),
            Wet => (Neither, Gear),
            Narrow => (Neither, Torch),
        };
        (State(r, a), State(r, b))
    }
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum Tool {
    Torch,
    Gear,
    Neither,
}

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum Region {
    Rocky,
    Narrow,
    Wet,
}

impl Region {
    fn from_erosion_level(n: usize) -> Self {
        match n {
            0 => Rocky,
            1 => Narrow,
            _ => Wet,
        }
    }
}
