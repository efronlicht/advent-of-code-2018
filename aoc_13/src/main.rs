#![feature(try_from)]
#![allow(non_camel_case_types)]

use std::collections::BTreeMap;
use std::convert::TryFrom;
use std::error;
use std::fmt;

type Result<T> = std::result::Result<T, Error>;
fn main() {
    println!("Hello, world!");
}

const TRACK_BOUND: usize = 160;
const INTERSECTION: char = '+';
const CURVE_RIGHT: char = '/';
const CURVE_LEFT: char = '\\';
const NORTH_SOUTH: char = '|';
const WEST_EAST: char = '-';
const NORTH: char = '^';
const SOUTH: char = 'v';
const EAST: char = '>';
const WEST: char = '<';
const EMPTY: char = ' ';
const CRASH: char = 'x';
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
enum Direction {
    North,
    South,
    East,
    West,
}

impl TryFrom<char> for Direction {
    type Error = Error;
    fn try_from(c: char) -> Result<Self> {
        Ok(match c {
            NORTH => Direction::North,
            SOUTH => Direction::South,
            EAST => Direction::East,
            WEST => Direction::West,
            _ => return Err(Error("unknown direction")),
        })
    }
}

impl TryFrom<char> for Track {
    type Error = Error;
    fn try_from(c: char) -> Result<Self> {
        Ok(match c {
            CURVE_RIGHT => Track::Curve(Turn::Right),
            CURVE_LEFT => Track::Curve(Turn::Left),
            EMPTY => Track::Empty,
            INTERSECTION => Track::Intersection,
            NORTH_SOUTH | NORTH | SOUTH => Track::NorthSouth,
            WEST_EAST | WEST | EAST => Track::WestEast,
            _ => return Err(Error("unknown track")),
        })
    }
}

impl Into<char> for Direction {
    fn into(self) -> char {
        match self {
            Direction::North => NORTH,
            Direction::South => SOUTH,
            Direction::East => EAST,
            Direction::West => WEST,
        }
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
struct State {
    carts: BTreeMap<Point, Cart>,
    tracks: Vec<Vec<Track>>,
}
#[test]
fn test_state() {
    const TEST_STATE: &str = include_str!("test_state");
    assert_eq!(TEST_STATE.parse::<State>().unwrap().save(), TEST_STATE);
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct Cart {
    direction: Direction,
    turn: Turn,
}

impl Cart {
    fn new(direction: Direction, turn: Turn) -> Self {
        Cart { direction, turn }
    }
}

impl TryFrom<char> for Cart {
    type Error = Error;
    fn try_from(c: char) -> Result<Self> {
        Ok(Cart {
            direction: Direction::try_from(c)?,
            turn: Turn::Left,
        })
    }
}

#[derive(Clone, PartialEq, Eq, Debug)]
struct Error(&'static str);
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl std::error::Error for Error {}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
enum Track {
    WestEast,
    NorthSouth,
    Curve(Turn),
    Intersection,
    Empty,
}

#[derive(Copy, Clone, Debug, PartialEq, PartialOrd, Ord, Eq)]
struct Point {
    y: usize,
    x: usize,
}

impl Point {
    fn move_direction(self, d: Direction) -> Point {
        let Point { x, y } = self;
        let (x, y) = match d {
            Direction::North => (x, y + 1),
            Direction::South => (x, y - 1),
            Direction::East => (x + 1, y),
            Direction::West => (x - 1, y),
        };
        Point { x, y }
    }
}

impl State {
    fn read(input: &str) -> Result<Self> {
        let mut tracks = vec![vec![Track::Empty; 160]; 160];
        let mut carts = BTreeMap::new();
        for (y, (col, line)) in tracks.iter_mut().zip(input.lines()).enumerate() {
            for (x, (track, c)) in col.iter_mut().zip(line.chars()).enumerate() {
                *track = Track::try_from(c)?;
                if let Ok(cart) = Cart::try_from(c) {
                    carts.insert(Point { x, y }, cart);
                }
            }
        }
        Ok(State { tracks, carts })
    }
    fn save(&self) -> String {
        let mut s = String::with_capacity(self.tracks.len() * (self.tracks[0].len() + 1) + 1); // items, plus newlines, plus extra newline at thee nd
        for (y, row) in self.tracks.iter().enumerate() {
            for (x, track) in row.iter().enumerate() {
                s.push(
                    if let Some(Cart { direction, .. }) = self.carts.get(&Point { y, x }) {
                        (*direction).into()
                    } else {
                        (*track).into()
                    },
                );
            }
            s.push('\n');
        }
        s.truncate(s.len() - 1); // last newline

        s
    }
}

fn pop_first<K: Ord + Clone, V>(tree: &mut BTreeMap<K, V>) -> Option<(K, V)> {
    let k = tree.keys().next()?.clone();
    let v = tree.remove(&k).unwrap();
    Some((k, v))
}
fn a(input: &str) -> Result<Point> {
    let inital_state = State::read(input)?;
    let State {
        mut carts,
        mut tracks,
    } = inital_state.clone();
    for tick in 0.. {
        let mut new_carts = BTreeMap::<Point, Cart>::new();

        while let Some((p, Cart { direction, turn })) = pop_first(&mut carts) {
            let p = p.move_direction(direction);

            let cart = match tracks[p.y][p.x] {
                Track::Intersection => Cart {
                    direction: direction.turn(turn),
                    turn: turn.next(),
                },
                Track::Curve(curve) => Cart::new(direction.turn(curve), turn),
                Track::NorthSouth | Track::WestEast => Cart::new(direction, turn),
                Track::Empty => panic!("should only move on rails!"),
            };

            if new_carts.insert(p, cart).is_some() || carts.contains_key(&p) {
                return Ok(p); // crash!
            }
        }
        carts = new_carts;
    }
    unreachable!()
}

impl std::str::FromStr for State {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self> {
        Self::read(s)
    }
}
impl<'a> Into<char> for Track {
    fn into(self) -> char {
        match self {
            Track::NorthSouth => NORTH_SOUTH,
            Track::WestEast => WEST_EAST,
            Track::Intersection => INTERSECTION,
            Track::Empty => EMPTY,
            Track::Curve(Turn::Left) => CURVE_LEFT,
            Track::Curve(Turn::Right) => CURVE_RIGHT,
            _ => panic!("curve straight shouldn't actually exist"),
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
enum Turn {
    Left,
    Right,
    Straight,
}

impl Default for Turn {
    fn default() -> Self {
        Turn::Left
    }
}

impl Turn {
    fn next(self) -> Self {
        match self {
            Turn::Left => Turn::Straight,
            Turn::Straight => Turn::Right,
            Turn::Right => Turn::Left,
        }
    }
}

impl Direction {
    fn turn(self, turn: Turn) -> Self {
        match (self, turn) {
            (d, Turn::Straight) => d,
            (Direction::North, Turn::Left) => Direction::West,
            (Direction::North, Turn::Right) => Direction::East,
            (Direction::West, Turn::Left) => Direction::South,
            (Direction::West, Turn::Right) => Direction::North,
            (Direction::South, Turn::Left) => Direction::East,
            (Direction::South, Turn::Right) => Direction::West,
            (Direction::East, Turn::Left) => Direction::North,
            (Direction::East, Turn::Right) => Direction::South,
        }
    }
}
