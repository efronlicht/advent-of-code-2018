#![feature(try_trait)]

#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate lazy_static;

use regex::Regex;
use std::collections::{BTreeMap, BTreeSet};
use std::fmt;
use std::fmt::{Display, Formatter, Write};
use std::str::FromStr;

const WEAK: &str = "weak to";
const IMMUNE: &str = "immune to";

type TargetMap = BTreeMap<usize, BTreeMap<usize, usize>>;
struct Targets {
    immune: TargetMap,
    infection: TargetMap,
    units: Vec<Unit>,
}

impl From<Vec<Unit>> for Targets {
    fn from(units: Vec<Unit>) -> Self {
        let (infected_indices, immune_indices): (Vec<usize>, Vec<usize>) =
            (0..units.len()).partition(|i| units[*i].infection);

        let calculate_targets = |attackers: &[usize], defenders: &[usize]| {
            let mut targets = TargetMap::new();
            for (i, attacker) in attackers.iter().cloned().map(|i| (i, &units[i])) {
                for (j, defender) in defenders.iter().cloned().map(|j| (j, &units[j])) {
                    targets
                        .entry(i)
                        .or_default()
                        .insert(j, attacker.calculate_damage(defender));
                }
            }
            targets
        };

        Targets {
            immune: calculate_targets(&immune_indices, &infected_indices),
            infection: calculate_targets(&infected_indices, &immune_indices),
            units,
        }
    }
}

#[test]
fn test_target() {
    let want = "Infection group 1 would deal defending group 1 185832 damage
Infection group 1 would deal defending group 2 185832 damage
Infection group 2 would deal defending group 2 107640 damage
Immune System group 1 would deal defending group 1 76619 damage
Immune System group 1 would deal defending group 2 153238 damage
Immune System group 2 would deal defending group 1 24725 damage
";
    let got = format!(
        "{}",
        Targets::from(parse_input(TEST_IMMUNE_INPUT, TEST_INFECTION_INPUT))
    );
    for (a, b) in want.lines().zip(got.lines()) {
        assert_eq!(a, b)
    }
}

impl Display for Targets {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        let write_group = |f: &mut Formatter, label: &str, attackers: &TargetMap| {
            for (k, v) in attackers.iter() {
                let attacker_id = self.units[*k].id;
                let defenders: BTreeMap<usize, usize> = v
                    .iter()
                    .map(|(i, damage)| (self.units[*i].id, *damage))
                    .collect();
                for (defender_id, damage) in defenders {
                    writeln!(
                        f,
                        "{} group {} would deal defending group {} {} damage",
                        label, attacker_id, defender_id, damage
                    )?;
                }
            }
            Ok(())
        };
        write_group(f, "Infection", &self.infection)?;
        write_group(f, "Immune System", &self.immune)
    }
}

fn print_state(units: &[Unit]) {
    let mut v = Vec::<u8>::with_capacity(100);
    write_state(&mut v, units).unwrap();
    let s = unsafe { String::from_utf8_unchecked(v) };
    print!("{}", s);
}

fn write_state<W: std::io::Write>(w: &mut W, units: &[Unit]) -> std::io::Result<()> {
    let (infection, immune): (Vec<_>, Vec<_>) = units.iter().partition(|u| u.infection);

    writeln!(w, "Immune System:")?;
    for Unit { id, units, .. } in immune {
        writeln!(w, "Group {} contains {} units", id, units)?;
    }
    writeln!(w, "Infection:")?;
    for Unit { id, units, .. } in infection {
        writeln!(w, "Group {} contains {} units", id, units)?;
    }
    Ok(())
}
/*
fn a(immune_input: &str, infection_input: &str) -> usize {
    let mut units = parse_input(immune_input, infection_input);
    for round in 0.. {
        units = units.into_iter().filter(|u| u.units > 0).collect(); // filter out dead units
        units.sort_by_key(|u| u.id);

        print_state(&units);

        if round % 10 == 0 {
            println!("\n\n ---- beginning of round {:5} ---", round);

            println!("{:#?}", units);
        }

        if units.iter().all(|u| !u.infection) {
            println!("game over at beginning of round {}: immune wins", round);
            break;
        } else if units.iter().all(|u| u.infection) {
            println!("game over at beginning of round {}: infection wins", round);
            break;
        }
        //Each fight consists of two phases: target selection and attacking.
        let targets = target_selection(&units);
        // At the end of the target selection phase, each group has selected zero or one groups to attack, and each group is being attacked by zero or one groups.
        // groups move in turn order.
        let turn_order = indices_by_key(&units, |u| -(u.initiative as isize));
        units = do_attacks(units, turn_order, targets);
    }
    println!("{:#?}", units);
    units.into_iter().map(|u| u.units).sum()
}

/// The attacking group chooses to target the group in the enemy army to which it would deal the most damage (after accounting for weaknesses and immunities, but not accounting for whether the defending group has enough units to actually receive all of that damage).
/// If an attacking group is considering two defending groups to which it would deal equal damage,
/// it chooses to target the defending group with the largest effective power;
/// if there is still a tie, it chooses the defending group with the highest initiative.
/// If it cannot deal any defending groups damage, it does not choose a target.
/// Defending groups can only be chosen as a target by one attacking group.
fn choose_targets(
    units: &[Unit],
    targeting_order: impl IntoIterator<Item = usize>,
) -> (BTreeMap<usize, usize>) {

}
*/
/// The defending group only loses whole units from damage; damage is always dealt in such a way that it kills the most units possible,
/// and any remaining damage to a unit that does not immediately kill it is ignored.
fn do_attacks(
    mut units: Vec<Unit>,
    turn_order: impl IntoIterator<Item = usize>,
    targets: BTreeMap<usize, usize>,
) -> Vec<Unit> {
    for i in turn_order {
        let attack = units[i].attack();
        if let Some(j) = targets.get(&i) {
            let j = *j;
            let killed = units[j].take_damage(attack);
            println!(
                "{} ({}) attacks {}, killing {}",
                units[i],
                if units[i].infection {
                    "infection"
                } else {
                    "immune"
                },
                units[j],
                killed,
            );
        } else {
            println!("{}, has no target", units[i]);
        }
    }
    units
}

impl fmt::Display for Unit {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(#{:02}, {} units, {} hp)", self.id, self.units, self.hp)
    }
}

#[test]
fn test_group_new() {
    let test_group: Unit = Unit {
        units: 3108,
        id: 1,
        infection: true,
        hp: 2902,
        damage: 7,
        attack_element: Element::COLD,
        weaknesses: Element::BLUDGEONING,
        immunities: Element::SLASHING | Element::FIRE,
        initiative: 13,
    };
    let input = concat!(
        r"3108 units each with 2902 hit points ",
        r"(weak to bludgeoning; immune to slashing, fire) ",
        r"with an attack that does 7 cold damage at initiative 13"
    );
    let want = assert_eq!(Unit::new(input, 1, true).unwrap(), test_group);
}

impl Unit {
    fn new(s: &str, id: usize, infection: bool) -> Result<Self, Error> {
        lazy_static! {
            static ref RE: Regex = Regex::new(concat!(
                r"(?P<units>\d+) units each with ",
                r"(?P<hp>\d+) hit points ",
                r"(?P<elements>\(.+\)?) with an attack that does ",
                r"(?P<damage>\d+) (?P<attack_element>\w+) damage at ",
                r"initiative (?P<initiative>\d+)"
            ))
            .unwrap();
        }
        let cap = RE.captures(s)?;

        Ok(Unit {
            units: cap["units"].parse()?,
            id,
            infection,
            hp: cap["hp"].parse()?,
            damage: cap["damage"].parse()?,
            attack_element: cap["attack_element"].parse()?,
            initiative: cap["initiative"].parse()?,
            weaknesses: weaknesses(&cap["elements"]),
            immunities: immunities(&cap["elements"]),
        })
    }
}

fn weaknesses(raw_elems: &str) -> BTreeMap<Element> {
    let mut elems = raw_elems[1..raw_elems.len() - 1] // trim parentheses
        .split(';')
        .map(str::trim)
        .filter(|s| !s.is_empty());
    match (elems.next(), elems.next()) {
        (Some(s), _) | (_, Some(s)) if s.starts_with(WEAK) => {
            Element::parse_many(s.trim_start_matches(WEAK))
        }
        _ => Element::NONE,
    }
}
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
enum Element {
    Fire,
    Cold,
    Radiation,
    Slashing,
    Bludgeoning,
}

#[derive(Clone, PartialEq, Eq, Debug)]
enum Error {
    UnknownType(String),
    NoMatch,
    IntError(std::num::ParseIntError),
}

impl From<std::num::ParseIntError> for Error {
    fn from(e: std::num::ParseIntError) -> Self {
        Error::IntError(e)
    }
}
impl From<std::option::NoneError> for Error {
    fn from(e: std::option::NoneError) -> Self {
        Error::NoMatch
    }
}
impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Error::UnknownType(s) => write!(f, "unknown type {}", s),
            Error::NoMatch => write!(f, "no match"),
            Error::IntError(e) => e.fmt(f),
        }
    }
}
impl std::error::Error for Error {}

impl FromStr for Element {
    type Err = Error;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "bludgeoning" => Element::BLUDGEONING,
            "slashing" => Element::SLASHING,
            "radiation" => Element::RADIATION,
            "cold" => Element::COLD,
            "fire" => Element::FIRE,
            s => return Err(Error::UnknownType(s.to_owned())),
        })
    }
}

impl Element {
    fn parse_many(s: &str) -> Self {
        s.split(',')
            .map(str::trim)
            .filter_map(|s| s.parse::<Element>().ok())
            .fold(Element::NONE, |folded, x| folded | x)
    }
}

#[derive(Clone, Debug)]
struct Unit {
    id: usize,
    hp: usize,
    initiative: usize,
    damage: usize,
    infection: bool,
    units: usize,
    attack_element: Element,
    weaknesses: Element,
    immunities: Element,
}

impl PartialEq for Unit {
    fn eq(&self, other: &Self) -> bool {
        self.infection == other.infection && self.id == other.id
    }
}
impl Eq for Unit {}

impl std::hash::Hash for Unit {
    fn hash<H: std::hash::Hasher>(&self, h: &mut H) {
        let Unit { infection, id, .. } = self;
        (infection, id).hash(h)
    }
}

#[test]
fn test_take_damage() {
    let defender = Unit {
        id: 0,
        hp: 5,
        units: 500,
        damage: 0,
        weaknesses: Element::SLASHING,
        immunities: Element::COLD,
        attack_element: Element::FIRE,
        infection: true,
        initiative: 1,
    };
    for (attack, killed) in vec![
        ((30, Element::COLD), 0),
        ((22, Element::RADIATION), 4),
        ((15, Element::SLASHING), 6),
    ] {
        assert_eq!(defender.clone().take_damage(attack), killed);
    }
}

type Attack = (usize, Element);

impl Unit {
    /// Each group also has an effective power: the number of units in that group multiplied by their attack damage.
    /// Groups never have zero or negative units; instead, the group is removed from combat.
    fn effective_power(&self) -> usize {
        self.units * self.damage
    }
    /// the attack of the unit and it's associated element
    fn attack(&self) -> Attack {
        (self.effective_power(), self.attack_element)
    }

    /// the defender takes the attack, killing off it's units (if any) and returning the number killed.
    ///
    /// The damage an attacking group deals to a defending group depends on the attacking group's attack type and the defending group's immunities and weaknesses.
    /// By default, an attacking group would deal damage equal to its effective power to the defending group.
    /// However, if the defending group is immune to the attacking group's attack type, the defending group instead takes no damage;
    /// if the defending group is weak to the attacking group's attack type, the defending group instead takes double damage

    /// The defending group only loses whole units from damage;
    /// ```
    /// // some fields hidden
    /// let defender = Unit {
    ///    #id: 0,
    ///    hp: 5,
    ///    units: 500,
    ///    weaknesses: Element::SLASHING,
    ///    immunities: Element::COLD,
    ///    #damage: 0,
    ///    #attack_element: Element::FIRE,
    ///    #infection: true,
    ///    #initiative: 1,
    /// };
    ///
    /// assert_eq!(defender.take_damage((22, Element::SLASHING)), 6);
    /// ```
    fn take_damage(&mut self, (mut damage, element): Attack) -> usize {
        damage = if self.immunities.contains(element) {
            0
        } else if self.weaknesses.contains(element) {
            damage * 2
        } else {
            damage
        };

        let killed = usize::min(self.units, damage / self.hp);
        self.units -= killed;
        killed
    }

    fn is_weak_to(&self, enemy: &Self) -> bool {
        self.weaknesses.contains(enemy.attack_element)
    }
    fn is_immune_to(&self, enemy: &Self) -> bool {
        self.immunities.contains(enemy.attack_element)
    }

    fn calculate_damage(&self, enemy: &Self) -> usize {
        if enemy.is_immune_to(&self) {
            0
        } else if enemy.is_weak_to(&self) {
            2 * self.effective_power()
        } else {
            self.effective_power()
        }
    }
}

/// return a vector containing `0..(slice.len())`, sorted by the key function on the corresponding member of
/// the slice
///
/// ```
/// let count_b = |s| s.chars().filter(|c| c == 'b').count();
/// assert_eq!(indices_by_key(&["abba", "aba", "abbba"], count_b) , vec![1, 0, 2]));
/// ```
#[inline]
fn indices_by_key<'a, F, B, T>(slice: &'a [T], mut f: F) -> Vec<usize>
where
    F: FnMut(&'a T) -> B,
    B: Ord,
{
    let mut indices: Vec<usize> = (0..slice.len()).collect();
    indices.sort_by_key(|i| f(&slice[*i]));
    indices
}

fn parse_input(immune_input: &str, infection_input: &str) -> Vec<Unit> {
    let (mut immune_team, mut infection_team) = (Vec::new(), Vec::new());
    let mut id = 1;
    for line in immune_input.lines() {
        if let Ok(immune_unit) = Unit::new(line, id, false) {
            immune_team.push(immune_unit);
            id += 1;
        }
    }

    id = 1;
    for line in infection_input.lines() {
        if let Ok(infection_unit) = Unit::new(line, id, true) {
            infection_team.push(infection_unit);
            id += 1;
        }
    }
    immune_team.into_iter().chain(infection_team).collect()
}

const IMMUNE_INPUT: &str = include_str!("immune.in");
const INFECTION_INPUT: &str = include_str!("infection.in");
const TEST_IMMUNE_INPUT: &str = include_str!("test_immune.in");
const TEST_INFECTION_INPUT: &str = include_str!("test_infection.in");
/*
fn main() {
    println!("aoc_2018_24_a => {}", a(IMMUNE_INPUT, INFECTION_INPUT))
}
*/
#[test]
fn test_write_state() {
    let want: Vec<u8> = b"Immune System:
Group 1 contains 17 units
Group 2 contains 989 units
Infection:
Group 1 contains 801 units
Group 2 contains 4485 units
"
    .to_vec();
    let mut got = Vec::new();
    write_state(
        &mut got,
        &parse_input(TEST_IMMUNE_INPUT, TEST_INFECTION_INPUT),
    )
    .unwrap();
    assert_eq!(
        String::from_utf8(want).unwrap(),
        String::from_utf8(got).unwrap()
    )
}
