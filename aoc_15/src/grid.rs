#[derive(Clone, PartialEq, Eq, Debug)]
pub struct Grid([[super::Square; 32]; 32]);

use super::{
    pathfind, Point, SolvedGraph,
    Square::{self, *},
};
use itertools::Itertools;
use std::collections::{BTreeSet, VecDeque};

impl Grid {
    pub fn save(&self) -> String {
        let mut s = String::with_capacity(33 * 33); // 32 chars + newlines + a bit of slack
        for row in self.0.iter() {
            for square in row.iter() {
                let c = match square {
                    Open => '.',
                    Goblin(_) => 'G',
                    Elf(_) => 'E',
                    Wall => '#',
                };
                s.push(c);
            }
            s.push('\n')
        }
        s.pop(); // remove the last newline
        s
    }

    pub fn load(s: &str) -> Self {
        let mut grid = Grid([[Open; 32]; 32]);
        for (y, line) in (0..).zip(s.lines()) {
            for (x, c) in (0..).zip(line.chars()) {
                let p = Point { y, x };
                grid[p] = match c {
                    'G' => Goblin(200),
                    'E' => Elf(200),
                    '#' => Wall,
                    '.' => Open,
                    _ => panic!("unknown square"),
                }
            }
        }
        grid
    }

    pub fn open_neighbors(&self, p: Point) -> Vec<Point> {
        p.neighbors().into_iter().filter(|q| self[*q] == Open).collect()
    }

    fn next_step_to_closest_goblin<'a>(&self, p: Point, goblins: impl IntoIterator<Item = &'a Point>) -> Option<Point> {
        if self.weakest_adjacent_goblin(p).is_some() {
            return None;
        }
        // pathfind: find the closest open square adjacent to an enemy, then the path
        // that leads there
        let goblin_adjacent_squares = goblins.into_iter().flat_map(|g| self.open_neighbors(*g));

        let SolvedGraph { distances, prev } = pathfind(self, p);
        goblin_adjacent_squares
            .map(|p| (distances[&p], p))
            .min()
            .and_then(|(dist, mut target)| {
                if dist == std::usize::MAX {
                    return None;
                }
                eprintln!("elf{} targets {}, moving to", p, target);
                for _ in 0..dist - 1 {
                    target = prev[&target];
                }
                eprintln!("{}", target);
                Some(target)
            })
    }

    fn next_step_to_closest_elf<'a>(&self, p: Point, elves: impl IntoIterator<Item = &'a Point>) -> Option<Point> {
        if self.weakest_adjacent_goblin(p).is_some() {
            return None;
        }
        // pathfind: find the closest open square adjacent to an enemy, then the path
        // that leads there
        let elf_adjacent_squares = elves.into_iter().flat_map(|e| self.open_neighbors(*e));
        let SolvedGraph { distances, prev } = super::pathfind(self, p);
        elf_adjacent_squares
            .map(|p| (distances[&p], p))
            .min()
            .and_then(|(dist, mut target)| {
                if dist == std::usize::MAX {
                    return None;
                }
                eprintln!("goblin{} targets {}, moving to", p, target);

                for _ in 0..dist - 1 {
                    target = prev[&target];
                }
                eprintln!("{}", target);

                Some(target)
            })
    }

    /// battle is over if only elves or only goblins remain
    pub fn battle_over(&self) -> bool { !self.vals().any(|s| (*s).is_goblin()) || !self.vals().any(|s| (*s).is_elf()) }

    fn weakest_adjacent_elf(&self, p: Point) -> Option<(u16, Point)> {
        p.neighbors()
            .into_iter()
            .filter_map(|q| if let Elf(hp) = self[q] { Some((hp, q)) } else { None })
            .min()
    }

    fn weakest_adjacent_goblin(&self, p: Point) -> Option<(u16, Point)> {
        p.neighbors()
            .into_iter()
            .filter_map(|q| if let Goblin(hp) = self[q] { Some((hp, q)) } else { None })
            .min()
    }

    pub fn next_round(&mut self) -> bool {
        eprintln!("beginning of round");
        let mut any_moves = false;
        let (mut turn_order, mut elves, mut goblins) = (VecDeque::new(), BTreeSet::new(), BTreeSet::new());
        for (p, square) in self.iter() {
            match square {
                Goblin(_) => {
                    turn_order.push_back(p);
                    goblins.insert(p);
                },
                Elf(_) => {
                    turn_order.push_back(p);
                    elves.insert(p);
                },
                _ => {},
            }
        }
        while let Some(p) = turn_order.pop_front() {
            if self.battle_over() {
                eprintln!("battle over!");

                return false;
            }

            any_moves = true;

            // pathfinding step
            let (p, hp, goblin) = match self[p] {
                Goblin(hp) => {
                    if self.weakest_adjacent_elf(p).is_some() {
                        eprintln!("goblin{} doesn't move", p);
                        (p, hp, true)
                    } else if let Some(target) = self.next_step_to_closest_elf(p, &elves) {
                        self[p] = Open;
                        self[target] = Goblin(hp);
                        goblins.remove(&p);
                        goblins.insert(target);
                        (target, hp, true)
                    } else {
                        (p, hp, true)
                    }
                },
                Elf(hp) => {
                    if self.weakest_adjacent_goblin(p).is_some() {
                        eprintln!("elf{} doesn't move", p);
                        (p, hp, false)
                    } else if let Some(target) = self.next_step_to_closest_goblin(p, &goblins) {
                        self[p] = Open;
                        self[target] = Elf(hp);
                        elves.remove(&p);
                        elves.insert(target);
                        (target, hp, false)
                    } else {
                        (p, hp, false)
                    }
                },
                _ => panic!("expected goblin or elf"),
            };

            // combat step

            if goblin {
                if let Some((enemy_hp, q)) = self.weakest_adjacent_elf(p) {
                    eprintln!(
                        "goblin({}) at {} strikes elf({}) at {:?} for 3 damage",
                        hp, p, enemy_hp, q
                    );

                    self[q] = if enemy_hp <= 3 {
                        eprintln!("elf at {} is dead!", q);

                        // unit is dead; we need to remove this position from the list of elves
                        elves.remove(&q);
                        turn_order
                            .iter()
                            .position(|r| *r == q)
                            .and_then(|i| turn_order.remove(i));
                        Open
                    } else {
                        Elf(enemy_hp - 3)
                    }
                }
            } else if let Some((enemy_hp, q)) = self.weakest_adjacent_goblin(p) {
                eprintln!(
                    "elf({}) at {} strikes goblin({}) at {} for 3 damage",
                    hp, p, enemy_hp, q
                );

                self[q] = if enemy_hp <= 3 {
                    // unit is dead; we need to remove this position from the turn order and the map
                    eprintln!("goblin at {} is dead!", q);
                    goblins.remove(&q);
                    turn_order
                        .iter()
                        .position(|r| *r == q)
                        .and_then(|i| turn_order.remove(i));
                    Open
                } else {
                    eprintln!("goblin at {} has {} hp remaining", q, enemy_hp - 3);

                    Goblin(enemy_hp - 3)
                }
            }
        }
        any_moves
    }

    pub fn iter(&self) -> impl Iterator<Item = (Point, &Square)> { Grid::points().zip(self.vals()) }

    pub fn vals(&self) -> impl Iterator<Item = &Square> { self.0.iter().flat_map(|row| row.iter()) }

    pub fn points() -> impl Iterator<Item = Point> { (0..32).cartesian_product(0..32).map(|(y, x)| Point { y, x }) }
}

#[test]
fn test_save() {
    let got = Grid::save(&Grid::load(super::INPUT));
    assert_eq!(super::INPUT, got);
}

impl std::ops::IndexMut<Point> for Grid {
    fn index_mut(&mut self, p: Point) -> &mut Self::Output { self.0.index_mut(p.y as usize).index_mut(p.x as usize) }
}
impl std::ops::Index<Point> for Grid {
    type Output = Square;

    fn index(&self, p: Point) -> &Self::Output { self.0.index(p.y as usize).index(p.x as usize) }
}
