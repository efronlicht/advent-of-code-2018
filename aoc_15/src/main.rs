mod grid;
#[cfg(test)]
use crate::grid::Grid;
use std::{
    collections::{BTreeMap, BTreeSet},
    fmt,
};

type SmallVec<T> = smallvec::SmallVec<[T; 4]>;
pub struct SolvedGraph<T: Ord> {
    distances: BTreeMap<T, usize>,
    prev: BTreeMap<T, T>,
}
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug)]
pub struct Point {
    y: u16,
    x: u16,
}

#[derive(Copy, Debug, Clone, PartialEq, Eq)]
pub enum Square {
    Open,
    Wall,
    Goblin(u16),
    Elf(u16),
}
pub const INPUT: &str = include_str!("in.a");

fn main() { a(INPUT) }
fn a(input: &str) {
    let mut grid = grid::Grid::load(input);
    let mut round = 0;
    while grid.next_round() {
        round += 1;
        eprintln!("round {}", round);
        eprintln!("{}", grid.save());
    }
    let winner: &str = if grid.vals().all(|s| s.is_goblin()) {
        "goblins"
    } else if !grid.vals().all(|s| !s.is_goblin()) {
        "elves"
    } else {
        panic!("game is tied!")
    };
    let survivors_hp: usize = grid
        .vals()
        .filter_map(|s| match s {
            Square::Elf(hp) | Square::Goblin(hp) => Some(*hp as usize),
            _ => None,
        })
        .sum();

    println!(
        "{} win!\nround{}, hp{} => {}",
        winner,
        round,
        survivors_hp,
        round * survivors_hp
    );
}

impl Point {
    pub fn up(self) -> Option<Self> {
        let Point { y, x } = self;
        if y > 0 {
            Some(Point { y: y - 1, x })
        } else {
            None
        }
    }

    pub fn down(self) -> Option<Self> {
        let Point { y, x } = self;
        if y < 32 {
            Some(Point { y: y + 1, x })
        } else {
            None
        }
    }

    pub fn left(self) -> Option<Self> {
        let Point { y, x } = self;
        if x > 0 {
            Some(Point { y, x: x - 1 })
        } else {
            None
        }
    }

    pub fn right(self) -> Option<Self> {
        let Point { y, x } = self;

        if x < 32 {
            Some(Point { y, x: x + 1 })
        } else {
            None
        }
    }

    pub fn neighbors(self) -> SmallVec<Point> {
        let mut neighbors = SmallVec::new();

        if let Some(up) = self.up() {
            neighbors.push(up);
        }
        if let Some(left) = self.left() {
            neighbors.push(left);
        }
        if let Some(down) = self.down() {
            neighbors.push(down);
        }

        if let Some(right) = self.right() {
            neighbors.push(right);
        }

        neighbors
    }
}

// implementation of Dijkstra's algorithm
fn pathfind(g: &grid::Grid, start: Point) -> SolvedGraph<Point> {
    let adjacency_lists: BTreeMap<Point, Vec<Point>> = grid::Grid::points()
        .filter(|p| *p == start || g[*p] == Square::Open)
        .map(|p| (p, g.open_neighbors(p)))
        .collect();

    let mut prev: BTreeMap<Point, Point> = BTreeMap::new();
    let mut unvisited: BTreeSet<Point> = adjacency_lists.keys().cloned().collect();
    let mut distances: BTreeMap<Point, usize> = adjacency_lists.keys().map(|k| (*k, std::usize::MAX)).collect();
    distances.insert(start, 0);

    while !unvisited.is_empty() {
        let current = *unvisited.iter().min_by_key(|p| distances[p]).unwrap();
        unvisited.remove(&current);
        let dist: usize = match distances[&current] {
            std::usize::MAX => continue,
            n => n + 1,
        };
        for neighbor in &adjacency_lists[&current] {
            let v = distances.get_mut(neighbor).unwrap();
            if dist < *v || dist <= *v && *neighbor < current {
                prev.insert(*neighbor, current);
                *v = dist;
            }
        }
    }
    SolvedGraph { distances, prev }
}

impl Square {
    pub fn is_goblin(self) -> bool {
        if let Square::Goblin(_) = self {
            true
        } else {
            false
        }
    }

    pub fn is_elf(self) -> bool {
        if let Square::Elf(_) = self {
            true
        } else {
            false
        }
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let Point { y, x } = self;
        write!(f, "{{y:{}, x:{}}}", y, x)
    }
}
