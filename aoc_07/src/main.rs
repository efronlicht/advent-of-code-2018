use std::collections::{BTreeMap, BTreeSet};

const INPUT: &str = include_str!("a.in");
const WORKERS: u32 = 5;
const DELAY_OFFSET: u8 = 60;
fn main() {
    println! {"aoc_2018_07_a => {}", a(INPUT)};
    println!("aoc_2018_07_b => {}", b(INPUT, WORKERS, DELAY_OFFSET));
}

fn a(input: &str) -> String {
    let mut reqs: BTreeMap<u8, BTreeSet<u8>> = BTreeMap::new();
    for (req, item) in parse_lines(input) {
        reqs.entry(item).or_default().insert(req);
        reqs.entry(req).or_default();
    }
    let mut output = Vec::<u8>::new();
    for _ in 0..reqs.len() {
        let k = reqs
            .iter()
            .find(|(_, v)| v.is_empty())
            .and_then(|(k, _)| Some(*k))
            .unwrap();
        output.push(k);
        reqs.remove(&k);
        for v in reqs.values_mut() {
            v.remove(&k);
        }
    }
    String::from_utf8(output).unwrap()
}

fn b(input: &str, mut workers: u32, delay_offset: u8) -> usize {
    let mut reqs: BTreeMap<u8, BTreeSet<u8>> = BTreeMap::new();
    for (req, item) in parse_lines(input) {
        reqs.entry(item).or_default().insert(req);
        reqs.entry(req).or_default();
    }

    let mut current = BTreeMap::<u8, u8>::new();

    let (mut remaining_jobs, mut elapsed) = (reqs.len(), 0);

    while remaining_jobs > 0 {
        //reader's note: the vecs `new_this_step` and `finished_this_step`
        // may look a little unusual, but it's here for a reason;
        // the borrow checker ensures that mutable access is exclusive access
        // we need to finish iterating through 'reqs' before we can modify it.
        // consider what would happen if we remove a key in a BTreeMap mid-iteration.
        let mut new_this_step = Vec::new();
        for (k, v) in reqs.iter() {
            match (v.len(), workers) {
                (_, 0) => break,
                (0, _) => {
                    new_this_step.push(*k);
                    workers -= 1;
                }
                _ => {}
            }
        }
        for k in new_this_step {
            reqs.remove(&k);
            current.insert(k, k - b'A' + 1 + delay_offset);
        }

        // every available job is being worked on, so we fast forward:
        let min_remaining = {
            let min = current.values().min().unwrap();
            *min
        };

        let mut finished_this_step = Vec::new();
        for (k, v) in current.iter_mut() {
            *v -= min_remaining;
            if *v == 0 {
                remaining_jobs -= 1;
                workers += 1;
                finished_this_step.push(*k)
            }
        }
        for k in finished_this_step {
            current.remove(&k);
            for v in reqs.values_mut() {
                v.remove(&k);
            }
        }
        elapsed += min_remaining as usize;
    }
    elapsed
}

fn parse_lines(s: &str) -> Vec<(u8, u8)> {
    let re = regex::Regex::new(r"Step (\w) must be finished before step (\w) can begin.").unwrap();
    s.lines()
        .map(str::trim)
        .map(|line| re.captures(line).unwrap())
        .map(|cap| {
            (
                cap[1].bytes().next().unwrap(),
                cap[2].bytes().next().unwrap(),
            )
        })
        .collect()
}

// this constant is only compiled into the binary in tests.
#[cfg(test)]
const TEST_INPUT: &str = "Step C must be finished before step A can begin.
Step C must be finished before step F can begin.
Step A must be finished before step B can begin.
Step A must be finished before step D can begin.
Step B must be finished before step E can begin.
Step D must be finished before step E can begin.
Step F must be finished before step E can begin.";
#[test]
fn test_a() {
    assert_eq!(a(TEST_INPUT), "CABDFE");
}
#[test]
fn test_b() {
    assert_eq!(b(TEST_INPUT, 2, 0), 15);
}

#[test]
fn test_parse_lines() {
    let input = "Step O must be finished before step W can begin.
Step S must be finished before step V can begin.
Step Z must be finished before step B can begin.";
    assert_eq!(
        parse_lines(input),
        vec![(b'O', b'W'), (b'S', b'V'), (b'Z', b'B')]
    );
}
