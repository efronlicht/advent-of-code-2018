const INPUT: usize = 640441;
fn main() {
    println! {"{:?}",a(INPUT)};
    println!("{:?}", b(&[6, 4, 0, 4, 4, 1]));
}

fn a(n: usize) -> Vec<u8> {
    let mut recipes: Vec<u8> = vec![3, 7];
    let (mut i, mut j) = (0, 1);

    while recipes.len() < (n + 10) {
        let (a, b) = (recipes[i], recipes[j]);
        let sum = a + b;
        if sum >= 10 {
            recipes.push(1);
            recipes.push(sum - 10);
        } else {
            recipes.push(sum);
        };
        i = (i + a as usize + 1) % recipes.len();
        j = (j + b as usize + 1) % recipes.len();
    }
    recipes[n..].to_vec()
}

fn b(want: &[u8]) -> usize {
    let mut recipes: Vec<u8> = vec![3, 7];
    let (mut i, mut j) = (0, 1);

    loop {
        let (a, b) = (recipes[i], recipes[j]);
        let sum = a + b;
        if sum >= 10 {
            recipes.push(1);
            if &recipes[recipes.len().saturating_sub(want.len())..] == want {
                break;
            }
        }
        recipes.push(sum % 10);
        if &recipes[recipes.len().saturating_sub(want.len())..] == want {
            break;
        }
        i = (i + a as usize + 1) % recipes.len();
        j = (j + b as usize + 1) % recipes.len();
    }
    recipes.len() - want.len()
}

#[test]
fn test_a() {
    assert_eq!(a(9), vec![5, 1, 5, 8, 9, 1, 6, 7, 7, 9]);
}

#[test]
fn test_b() {
    assert_eq!(b(&[5, 1, 5, 8, 9]), 9);
    assert_eq!(b(&[9, 2, 5, 1, 0]), 18);
}
