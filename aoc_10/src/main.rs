use regex::Regex;
fn main() {
    a();
}

const INPUT: &str = include_str!("a.in");
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
struct Point {
    x: i32,
    y: i32,
    dx: i32,
    dy: i32,
}

fn a() {
    let initial_points = parse_lines(INPUT);
    let (mut prev_entropy, mut current_entropy) = (usize::max_value(), usize::max_value() - 1);
    let mut t = 0;
    let mut points = initial_points.clone();
    while current_entropy < prev_entropy {
        t += 1;
        prev_entropy = points
            .iter()
            .map(|p| points.iter().map(|q| p.dist(q)).sum::<usize>())
            .sum::<usize>();

        std::mem::swap(&mut prev_entropy, &mut current_entropy);
        for p in &mut points {
            p.x += p.dx;
            p.y += p.dy;
        }
    }

    for t in (t - 5)..(t + 5) {
        println!("{}", t);
        plot_points(
            initial_points
                .iter()
                .cloned()
                .map(|p| p.at_time(t))
                .collect(),
        );
        println!("########################################################################\n\n\n")
    }
}

fn plot_points(points: Vec<Point>) {
    let ((min_y, max_y), (min_x, max_x)) = (
        (
            points.iter().map(|p| p.y).min().unwrap(),
            points.iter().map(|p| p.y).max().unwrap(),
        ),
        (
            points.iter().map(|p| p.x).min().unwrap(),
            points.iter().map(|p| p.x).max().unwrap(),
        ),
    );

    let mut lines: Vec<Vec<u8>> =
        vec![vec![b' '; (max_x - min_x + 1) as usize]; (max_y - min_y + 1) as usize];

    for Point { x, y, .. } in points {
        let y = (y - min_y) as usize;
        let x = (x - min_x) as usize;
        lines[y][x] = b'#';
    }

    for line in lines {
        println!("{}", String::from_utf8(line).unwrap());
    }
}
impl Point {
    fn at_time(self, t: i32) -> Point {
        let Point { x, y, dx, dy } = self;
        Point {
            x: x + (t * dx),
            y: y + (t * dy),
            dx,
            dy,
        }
    }
    fn dist(&self, other: &Point) -> usize {
        (i32::abs(self.x - other.x) + i32::abs(self.y - other.y)) as usize
    }
}

fn parse_lines(s: &str) -> Vec<Point> {
    // (-?\d+) matches one or more digits, with an optional negative sign
    // we use the non-greedy matcher .*? rather than the greedy matcher to keep the negative sign if it exists
    let re = Regex::new(r".*?(-?\d+).*?(-?\d+).*?(-?\d+).*?(-?\d+)").unwrap();
    s.lines()
        .map(|line| re.captures(line).unwrap())
        .map(|cap| Point {
            x: cap[1].parse().unwrap(),
            y: cap[2].parse().unwrap(),
            dx: cap[3].parse().unwrap(),
            dy: cap[4].parse().unwrap(),
        })
        .collect()
}
#[test]
fn test_parse_lines() {
    assert_eq!(
        parse_lines("position=<-3, 11> velocity=< 1, -2>\nposition=< 3, -2> velocity=<-1,  1>"),
        vec![
            Point {
                x: -3,
                y: 11,
                dx: 1,
                dy: -2
            },
            Point {
                x: 3,
                y: -2,
                dx: -1,
                dy: 1
            }
        ]
    );
}
