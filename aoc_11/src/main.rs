const INPUT: i32 = 6392;
use itertools::Itertools;
use std::time::Instant;

fn main() {
    let start = Instant::now();
    let (x, y) = a(INPUT);
    let elapsed = start.elapsed();

    println!("aoc_2018_11_a => ({},{})", x, y);
    println!(
        "ran in {:01}.{:03}s",
        elapsed.as_secs(),
        elapsed.subsec_millis()
    );

    let start = Instant::now();
    let ((x, y), size) = b2(INPUT);
    println!("aoc_2018_11_b => ({}, {}, {})", x, y, size);
    let elapsed = start.elapsed();
    println!(
        "ran in {:01}.{:.03}s",
        elapsed.as_secs(),
        elapsed.subsec_millis()
    );

    println!("")
}

fn a(serial: i32) -> (i32, i32) {
    let (_, (x, y)) = max_for_size(serial, 3);
    (x, y)
}

fn b2(serial: i32) -> ((usize, usize), usize) {
    let mut max_k: ((usize, usize), usize) = ((0, 0), 1);
    let mut max_v = power(1, 1, 1);

    // we know initial values are bounded to the range [-5, 4] since they're equal to the hundreth's digit of
    // some number, -5, so we use an i8 to represent them
    // putting this on the stack rather than using two vecs saves about a third of the runtime;
    let initial = {
        let mut initial = [[0_i8; 300]; 300];
        for (x, col) in (1..).zip(initial.iter_mut()) {
            for (y, v) in (1..).zip(col.iter_mut()) {
                *v = power(x, y, serial) as i8;
                if i32::from(*v) > max_v {
                    max_k = ((x, y), 1);
                    max_v = i32::from(*v);
                }
            }
        }
        initial
    };
    let mut grid: [[i32; 300]; 300] = [[0; 300]; 300];
    for (i, j) in (0..300).cartesian_product(0..300) {
        grid[i][j] = i32::from(initial[i][j]);
    }

    for size in 2..=300 {
        let bound = 300 - size;

        for (i, col) in grid[..bound].iter_mut().enumerate() {
            for (j, v) in col[..bound].iter_mut().enumerate() {
                let edges = {
                    let (right, bottom) = (i + size, j + size);
                    let right_edge: i32 = (i..right).map(|x| i32::from(initial[x][bottom])).sum();
                    let bottom_edge: i32 = (j..bottom).map(|y| i32::from(initial[right][y])).sum();
                    let corner = i32::from(initial[right][bottom]);
                    right_edge + bottom_edge + corner
                };

                *v += edges;
                if *v > max_v {
                    max_k = ((i + 1, j + 1), size + 1);
                    max_v = *v;
                }
            }
        }
    }

    max_k
}

fn max_for_size(input: i32, size: i32) -> (i32, (i32, i32)) {
    debug_assert!(size > 0);
    (1..=(300 - size + 1))
        .cartesian_product(1..=(300 - size + 1))
        .map(|(x, y)| {
            let gridsum = (x..y + size)
                .cartesian_product(y..y + size)
                .map(|(x_i, y_i)| power(x_i as usize, y_i as usize, input))
                .sum::<i32>();
            (gridsum, (x, y))
        })
        .max()
        .unwrap()
}

/// power must be between -5 and 4, since it's the hundreth's digit of some number
/// this would be cleaner if I could use "let" statements but I wanted it to be a `const fn`
const fn power(x: usize, y: usize, serial: i32) -> i32 {
    ((((((x as i32 + 10) * y as i32) + serial) * (x as i32 + 10) % 1000) / 100) - 5)
}
#[test]
fn test_power() {
    let tt = vec![
        ((122, 79, 57), -5),
        ((217, 196, 39), 0),
        ((101, 153, 71), 4),
    ];
    for ((x, y, serial), want) in tt {
        assert_eq!(power(x, y, serial), want);
    }
}

#[test]
fn test_b() {
    let tt = vec![(18, ((90, 269), 16)), (42, ((232, 251), 12))];
    for (serial, res) in tt {
        assert_eq!(b2(serial), res);
    }
}
