use std::collections::BTreeMap;
const INPUT: &'static str = include_str!("a.in");

fn main() {
    println!("aoc_2018_02_a => {}", a(INPUT));
    println!("aoc_2018_02_b => {}", b(INPUT));
}

/// To make sure you didn't miss any, you scan the likely candidate boxes again,
/// counting the number that have an ID containing exactly two of any letter
/// and then separately counting those with exactly three of any letter.
/// You can multiply those two counts together to get a rudimentary checksum and compare it to what your device predicts.
fn a(input: &str) -> usize {
    let line_counts = input.split_whitespace().map(count_lowercase_letters);
    let (twos, threes) = line_counts.fold((0, 0), |(twos, threes), count| {
        (
            twos + usize::from(count.iter().any(|n| *n == 2)),
            threes + usize::from(count.iter().any(|n| *n == 3)),
        )
    });

    twos * threes
}
// using a [u8; 26] instead of a map or set is faster and more efficient,
// since we know there's only 26 possibilities
// and a totally totally unneccessary optimization but sssshhh
fn count_lowercase_letters(s: &str) -> [u8; 26] {
    let mut counts: [u8; 26] = [0; 26];
    let offset_from_a = s.bytes().filter_map(|b| match b {
        b @ b'a'..=b'z' => Some((b - b'a') as usize),
        _ => None,
    });
    for i in offset_from_a {
        counts[i] += 1
    }
    counts
}

/// The IDs abcde and axcye are close, but they differ by two characters (the second and fourth).
/// However, the IDs fghij and fguij differ by exactly one character, the third (h and u).
/// Those must be the correct boxes.
/// What letters are common between the two correct box IDs?
/// (In the example above, this is found by removing the differing character from either ID, producing fgij.)
fn b(input: &str) -> String {
    let lines: Vec<&str> = input.lines().map(|s| s.trim()).collect();
    for (i, a) in lines.iter().enumerate() {
        if let Some(b) = lines[i + 1..].iter().find(|b| char_distance(a, b) == 1) {
            return shared_chars(a, b);
        }
    }
    unreachable!()
}
fn shared_chars(a: &str, b: &str) -> String {
    Iterator::zip(a.chars(), b.chars())
        .filter(|(a, b)| a == b)
        .map(|(a, _)| a)
        .collect()
}

fn char_distance(a: &str, b: &str) -> usize {
    assert!(a.len() == b.len());
    let chars = a.chars().zip(b.chars());
    chars.filter(|(a, b)| a != b).count()
}

#[test]
fn test_a() {
    let input =
        concat!("abcdef\n", "bababc\n", "abbcde\n", "abcccd\n", "aabcdd\n", "abcdee\n", "ababab");

    assert_eq!(a(input), 12);
}
#[test]
fn test_b() {
    let input = concat!("abcde\n", "fghij\n", "klmno\n", "pqrst\n", "fguij\n", "axcye\n", "wvxyz");
    let want = "fgij";
    assert_eq!(b(input), want)
}
