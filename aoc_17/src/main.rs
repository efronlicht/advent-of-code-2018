use regex::Regex;
use std::ops::RangeInclusive;
fn main() {
    println!("Hello, world!");
}

type Range = RangeInclusive<usize>;
type Grid = Vec<Vec<Square>>;
#[derive(Copy, Clone, PartialEq, Eq)]
enum Square {
    Clay,
    DrySand,
    WetSand,
    Spring,
    Water,
}

fn next(grid: &Grid) -> Grid {
    let mut next = grid.clone();
    let (ymax, xmax) = (grid.len(), grid[0].len());
    for y in (1..ymax)
        // edges
        if grid[]

        for x in (1..xmax-1) {
            
        }
    }
}

fn parse_input(input: &str) -> Vec<Vec<Square>> {
    let mut ranges = Vec::<(usize, usize, usize, usize)>::new();
    let x_range = Regex::new(r"x=(\d+)..(\d+)").unwrap();
    let y_range = Regex::new(r"y=(\d+)..(\d+)").unwrap();
    let x_singular = Regex::new(r"x=(\d+)").unwrap();
    let y_singular = Regex::new(r"x=(\d+)").unwrap();
    for line in input.lines() {
        let (xmin, xmax) = match x_range.captures(line) {
            Some(x) => (x[1].parse().unwrap(), x[2].parse().unwrap()),
            None => {
                let x = x_singular.captures(line).unwrap()[1].parse().unwrap();
                (x, x)
            }
        };
        let (ymin, ymax) = match y_range.captures(line) {
            Some(y) => (y[1].parse().unwrap(), y[2].parse().unwrap()),
            None => {
                let y = y_singular.captures(line).unwrap()[1].parse().unwrap();
                (y, y)
            }
        };
        ranges.push((xmin, xmax, ymin, ymax));
    }
    let min_x = *ranges.iter().map(|(xmin, _, _, _)| xmin).min().unwrap();
    let max_x = *ranges.iter().map(|(_, xmax, _, _)| xmax).max().unwrap() + 1;

    let min_y = *ranges.iter().map(|(_, _, ymin, _)| ymin).min().unwrap();
    let max_y = *ranges.iter().map(|(_, _, _, ymax)| ymax).max().unwrap() + 1;

    let (x, y) = (max_x - min_x, max_y - min_y);
    let mut grid = vec![vec![Square::Water; x]; y];

    for (x0, x1, y0, y1) in ranges {
        let (x0, x1, y0, y1) = (x0 - min_x, x1 - min_x, y0 - min_x, y1 - min_x);
        for row in &mut grid[y0..=y1] {
            for square in &mut row[x0..=x1] {
                *square = Square::Clay;
            }
        }
    }
    grid[0][500 - min_x] = Square::Water;
    grid
}
