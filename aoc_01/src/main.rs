fn main() {
    println!("aoc_2018_01_a => {}", a_01(INPUT));
    println!("aoc_2018_01_b => {}", b_01(INPUT));
}

const INPUT: &str = include_str!("input.txt");

fn a_01(input: &str) -> isize {
    input
        .split_whitespace()
        .filter_map(|s| s.parse::<isize>().ok())
        .sum()
}

fn b_01(input: &str) -> isize {
    let mut seen = std::collections::BTreeSet::new();
    let mut frequency = 0;
    seen.insert(frequency);

    input
        .split_whitespace()
        .filter_map(|s| s.parse::<isize>().ok())
        .cycle()
        .find(|n| {
            frequency += n;
            !seen.insert(frequency)
        });
    frequency
}
#[test]
fn test_a_01() {
    let tt = vec![("+1 +1 +1", 3), ("+1 +1 -2", 0), ("-1 -2 -3", -6)];
    for (input, want) in tt {
        assert_eq!(a_01(input), want);
    }
}

#[test]
fn test_b_01() {
    let tt = vec![
        ("+3 +3 +4 -2 -4", 10),
        ("-6 +3 +8 +5 -6", 5),
        ("+7 +7 -2 -7 -4", 14),
    ];
    for (input, want) in tt {
        assert_eq!(b_01(input), want);
    }
}
