fn main() {
    let parsed = parse(INPUT);
    assert_eq!(parsed.len(), 1080);
    println!("aoc_2018_25_a => {}", merge(&parsed));
}

use std::collections::{BTreeMap, BTreeSet};
type Point = (i16, i16, i16, i16);
const INPUT: &str = include_str!("a.in");

pub trait Poppable {
    type Item;
    fn pop(&mut self) -> Option<Self::Item>;
}

impl<K: Ord + Copy> Poppable for BTreeSet<K> {
    type Item = K;
    fn pop(&mut self) -> Option<Self::Item> {
        if let Some(k) = deref_opt(self.iter().next()) {
            self.remove(&k);
            Some(k)
        } else {
            None
        }
    }
}

impl<K: Ord + Copy, V> Poppable for BTreeMap<K, V> {
    type Item = (K, V);
    fn pop(&mut self) -> Option<Self::Item> {
        if let Some(k) = deref_opt(self.keys().next()) {
            Some((k, self.remove(&k).unwrap()))
        } else {
            None
        }
    }
}
#[inline]
fn manhattan(a: Point, b: Point) -> i16 {
    i16::abs(a.0 - b.0) + i16::abs(a.1 - b.1) + i16::abs(a.2 - b.2) + i16::abs(a.3 - b.3)
}

#[inline]
fn deref_opt<T: Copy>(opt: Option<&T>) -> Option<T> {
    if let Some(t) = opt {
        Some(*t)
    } else {
        None
    }
}

fn initial_constellations(points: &[Point]) -> BTreeMap<Point, BTreeSet<Point>> {
    let mut connections: BTreeMap<Point, BTreeSet<Point>> = BTreeMap::new();

    for p in points.iter().cloned() {
        for q in points.iter().cloned() {
            if manhattan(p, q) <= 3 {
                connections.entry(p).or_default().insert(q);
            }
        }
    }
    connections
}

fn merge(points: &[Point]) -> usize {
    let mut connections = initial_constellations(points);

    let mut merge_on_last_pass = true;
    while merge_on_last_pass {
        merge_on_last_pass = false;
        let mut next = BTreeMap::new();
        while let Some((p, mut constellation)) = connections.pop() {
            // we look for all constellations that contain a point in p's constellation.
            let keys = constellation.iter().cloned().collect::<Vec<_>>();
            for q in keys {
                if let Some(neighboring_constellation) = connections.get_mut(&q) {
                    let prev_len = constellation.len();
                    constellation.extend(neighboring_constellation.iter());
                    *neighboring_constellation = constellation.clone();
                    if constellation.len() > prev_len {
                        merge_on_last_pass = true;
                    }
                }
            }
            let min_key = *constellation.iter().next().unwrap();

            next.insert(min_key, constellation);
        }
        connections = next;
    }
    connections.len()
}

fn parse_line(line: &str) -> Option<Point> {
    let mut s = line.split(',').map(|p| p.trim().parse::<i16>().unwrap());
    if let (Some(w), Some(x), Some(y), Some(z)) = (s.next(), s.next(), s.next(), s.next()) {
        Some((w, x, y, z))
    } else {
        None
    }
}

fn parse(s: &str) -> Vec<Point> {
    s.lines().filter_map(parse_line).collect()
}

#[test]
fn test_run() {
    const INPUT_0: &str = concat!(
        "-1,2,2,0\n",
        "0,0,2,-2\n",
        "0,0,0,-2\n",
        "-1,2,0,0\n",
        "-2,-2,-2,2\n",
        "3,0,2,-1\n",
        "-1,3,2,2\n",
        "-1,0,-1,0\n",
        "0,2,1,-2\n",
        "3,0,0,0"
    );
    const INPUT_1: &str = concat!(
        "1,-1,-1,-2\n",
        "-2,-2,0,1\n",
        "0,2,1,3\n",
        "-2,3,-2,1\n",
        "0,2,3,-2\n",
        "-1,-1,1,-2\n",
        "0,-2,-1,0\n",
        "-2,2,3,-1\n",
        "1,2,2,0\n",
        "-1,-2,0,-2"
    );

    const INPUT_2: &str = concat!(
        "1,-1,0,1\n",
        "2,0,-1,0\n",
        "3,2,-1,0\n",
        "0,0,3,1\n",
        "0,0,-1,-1\n",
        "2,3,-2,0\n",
        "-2,2,0,0\n",
        "2,-2,0,-1\n",
        "1,-1,0,-1\n",
        "3,2,0,2"
    );
    let tt = vec![(INPUT_0, 4), (INPUT_1, 8), (INPUT_2, 3)];
    for (input, want) in tt {
        let got = merge(&parse(input));
        assert_eq!(got, want);
    }
}
#[test]
fn test_parse() {
    let input = concat!("4,1,7,-2\n", "1,0,3,-1\n", "-8,8,7,-3");
    let want = vec![(4, 1, 7, -2), (1, 0, 3, -1), (-8, 8, 7, -3)];
    assert_eq!(parse(input), want);
}
