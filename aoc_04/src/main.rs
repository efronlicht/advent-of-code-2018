const INPUT: &str = include_str!("a.in");
extern crate chrono;
use chrono::{NaiveDateTime, Timelike};
use std::collections::BTreeMap;
use std::str::FromStr;

fn main() {
    println!("aoc_2018_04_a => {}", a(INPUT));
    println!("aoc_2018_04_b => {}", b(INPUT));
}

type SleepSchedule = [u16; 60];
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Event {
    ShiftStart(u32),
    WakeUp,
    FallAsleep,
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Record {
    time: NaiveDateTime,
    event: Event,
}
fn a(input: &str) -> usize {
    let (sleepiest_guard, sleep_schedule) = sleep_times(input)
        .into_iter()
        .max_by_key(|(_, v)| v.iter().map(|n| u32::from(*n)).sum::<u32>())
        .unwrap();
    let minute = (0..60)
        .max_by_key(|minute| sleep_schedule[*minute as usize]) // we prioritize earlier times
        .unwrap();

    minute * sleepiest_guard as usize
}

/// --- Part Two ---
///Strategy 2: Of all guards, which guard is most frequently asleep on the same minute?
///In the example above, Guard #99 spent minute 45 asleep more than any other guard or minute
/// - three times in total. (In all other cases, any guard spent any minute asleep at most twice.)
///What is the ID of the guard you chose multiplied by the minute you chose?
/// (In the above example, the answer would be 99 * 45 = 4455.)
fn b(input: &str) -> usize {
    let sleep_schedules = sleep_times(input);

    let mut sleepiest_minute = 0;
    let (mut sleepiest_guard, mut max_sleep_time) = find_sleepiest(0, &sleep_schedules);
    for minute in 1..60 {
        let (guard, time) = find_sleepiest(minute, &sleep_schedules);
        if time > max_sleep_time {
            sleepiest_guard = guard;
            max_sleep_time = time;
            sleepiest_minute = minute;
        }
    }
    sleepiest_guard as usize * sleepiest_minute as usize
}

fn find_sleepiest(minute: usize, sleep_schedules: &BTreeMap<u32, SleepSchedule>) -> (u32, u16) {
    sleep_schedules
        .iter()
        .map(|(guard, sched)| (*guard, sched[minute]))
        .max_by_key(|(guard, sleep_time)| (*sleep_time, -(*guard as isize)))
        .unwrap()
}

fn sleep_times(input: &str) -> BTreeMap<u32, SleepSchedule> {
    let mut records: Vec<Record> = input.lines().map(|s| s.parse().unwrap()).collect();
    records.sort_by_key(|r| r.time);
    let mut sleep_times: BTreeMap<u32, SleepSchedule> = BTreeMap::new();

    let mut records = records.into_iter();
    let (mut last_event_time, mut current_guard) = if let Some(Record {
        time,
        event: Event::ShiftStart(guard),
    }) = records.next()
    {
        (time, guard)
    } else {
        panic!("first event should be a guard starting shift")
    };
    for Record { time, event } in records {
        match event {
            Event::ShiftStart(guard) => current_guard = guard,
            Event::WakeUp if time.hour() == 0 => {
                assert!(last_event_time < time);

                let current_minute = time.minute();
                let sleep = sleep_times.entry(current_guard).or_insert([0; 60]);
                let sleep_start_minute = if last_event_time.hour() == 0 {
                    last_event_time.minute()
                } else {
                    0
                };
                for i in sleep_start_minute..current_minute {
                    sleep[i as usize] += 1
                }
            }
            _ => {}
        }
        last_event_time = time;
    }
    sleep_times
}

impl PartialOrd for Record {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.time.partial_cmp(&other.time)
    }
}

impl FromStr for Record {
    type Err = <NaiveDateTime as FromStr>::Err;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.trim().trim_left_matches('[').split(']').map(str::trim);
        if let (Some(raw_date_time), Some(event)) = (split.next(), split.next()) {
            Ok(Record {
                time: NaiveDateTime::parse_from_str(raw_date_time, "%Y-%m-%d %H:%M")?,
                event: if event.contains("begins shift") {
                    Event::ShiftStart(
                        event
                            .chars()
                            .filter(|c| char::is_digit(*c, 10))
                            .collect::<String>()
                            .parse()
                            .unwrap(),
                    )
                } else if event.contains("falls asleep") {
                    Event::FallAsleep
                } else if event.contains("wakes up") {
                    Event::WakeUp
                } else {
                    panic!("unknown event type")
                },
            })
        } else {
            panic!("bad record")
        }
    }
}
#[test]
fn test_parse() {
    assert_eq!(
        "[1518-11-01 00:00] Guard #10 begins shift"
            .parse::<Record>()
            .unwrap(),
        Record {
            time: NaiveDateTime::new(
                chrono::NaiveDate::from_ymd(1518, 11, 1),
                chrono::NaiveTime::from_hms(0, 0, 0)
            ),
            event: Event::ShiftStart(10),
        }
    )
}

#[cfg(test)]
const TEST_INPUT: &str = "[1518-11-01 00:00] Guard #10 begins shift
[1518-11-01 00:05] falls asleep
[1518-11-01 00:25] wakes up
[1518-11-01 00:30] falls asleep
[1518-11-01 00:55] wakes up
[1518-11-01 23:58] Guard #99 begins shift
[1518-11-02 00:40] falls asleep
[1518-11-02 00:50] wakes up
[1518-11-03 00:05] Guard #10 begins shift
[1518-11-03 00:24] falls asleep
[1518-11-03 00:29] wakes up
[1518-11-04 00:02] Guard #99 begins shift
[1518-11-04 00:36] falls asleep
[1518-11-04 00:46] wakes up
[1518-11-05 00:03] Guard #99 begins shift
[1518-11-05 00:45] falls asleep
[1518-11-05 00:55] wakes up";

#[test]
fn test_a() {
    assert_eq!(a(TEST_INPUT), 240);
}

#[test]
fn test_b() {
    assert_eq!(b(TEST_INPUT), 4455)
}
