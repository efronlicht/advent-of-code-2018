#![feature(type_ascription)]
use regex::Regex;

use std::cmp::Ordering;
fn main() { println!("aoc_2018_20_a => {}", a(INPUT)) }
fn a(input: &str) -> usize { parse(input).unwrap().max_len() }

const INPUT: &str = include_str!("a.in");
const REGEXP: &str = r"^\^?([NSEW]*)(?:\(([NSEW()|]*)\))?([NSEW]*)\$?$";
#[derive(Clone, Debug, PartialEq, Eq)]
enum RE {
    Single(String),
    MandatoryBranch(Vec<RE>),
    OptionalBranch(Vec<RE>),
    Chain(Box<(RE, RE)>),
    Start(Box<RE>),
}

fn subgroup_end(s: &str, mut start: usize, mut depth: usize) -> Option<usize> {
    assert_eq!(s.as_bytes()[start], b'(');
    start += 1;
    for (j, b) in s[start..].bytes().enumerate() {
        match b {
            b'(' => depth += 1,
            b')' if depth == 0 => return Some(start + j),
            b')' => depth -= 1,
            _ => {},
        }
    }
    None
}
impl RE {
    fn max_len(&self) -> usize {
        match self {
            RE::Single(s) => s.len(),
            RE::MandatoryBranch(branches) | RE::OptionalBranch(branches) => {
                branches.iter().map(RE::max_len).max().unwrap_or_default()
            },
            RE::Chain(items) => items.iter().map(RE::max_len).sum(),
            RE::Start(re) => re.max_len(),
        }
    }
}

#[test]
fn test_max_len() {
    for (input, want) in vec![
        (r"^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$", 23),
        (r"^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$", 31),
        (r"^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$", 18),
    ] {
        let re = parse(input, 0, 0).unwrap();
        assert_eq!(re.max_len(), want)
    }
}

#[test]
fn test_re() {
    for input in vec![
        r"^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$",
        r"^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$",
        r"^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$",
    ] {
        let re = parse(input, 0, 0).unwrap();
        assert_eq!(re.as_str(), input);

        assert_eq!(parse(INPUT, 0, 0).unwrap().as_str(), INPUT);
    }
}
impl RE {
    fn as_str(&self) -> String {
        match self {
            RE::Start(regex) => format!("^{}$", regex.as_str()),
            RE::Single(directions) => directions.to_string(),
            RE::MandatoryBranch(branches) => format!(
                "({})",
                branches.iter().map(RE::as_str).collect::<Vec<String>>().join("|")
            ),
            RE::OptionalBranch(branches) => format!(
                "({}|)",
                branches.iter().map(RE::as_str).collect::<Vec<String>>().join("|")
            ),
            RE::Chain(regexes) => regexes.iter().map(RE::as_str).collect(),
        }
    }
}

fn parse_branch(s: &str) -> RE {
    assert!(s.starts_with('(') && s.ends_with(')'));
    let mut depth: usize = 0;
    let mut start = 0;
    let mut branches = Vec::new();
    let mut bytes = s.bytes().enumerate().peekable();

    while let Some((i, b)) = bytes.next() {
        match b {
            b'(' => depth += 1,
            b')' => {
                depth = depth
                    .checked_sub(1)
                    .unwrap_or_else(|| panic!(format!("unmatched paren: at {}", i)));
            },
            b'|' if depth == 0 => {
                let branch = &s[start..i];
                branches.push(parse(branch, 0, 0).unwrap());
                if let Some((_, b')')) = bytes.peek() {
                    return RE::OptionalBranch(branches);
                }
                start = i + 1;
            },

            b'|' | b'N' | b'E' | b'S' | b'W' => {},
            _ => panic!("unknown char"),
        }
    }
    if start != s.len() {
        branches.push(parse(&s[start..], 0, 0).unwrap());
    }

    RE::MandatoryBranch(branches)
}

fn parse(s: &str, start: usize, depth: usize) -> Option<RE> {
    let mut i = start;
    let bytes = s.as_bytes();
    while i < s.len() {
        match bytes[i] {
            b'(' => {
                let end = subgroup_end(s, start, depth);
                vec::
        }
    }
}

// #![feature(try_from)]
// fn main() {
// println!("Hello, world!");
// }
// use std::{convert::TryFrom, str::FromStr};
//
// #[derive(Clone, Debug, PartialEq, Eq)]

// #[derive(Copy, Clone, PartialEq, Eq, Debug)]
// enum ErrorKind {
// UnknownByte(u8),
// HangingLeftParen,
// MismatchedRightParen,
// BranchOutsideGroup,
// }
//
// #[derive(Copy, Clone, PartialEq, Eq, Debug)]
// enum Direction {
// North,
// South,
// East,
// West,
// }
//
// #[derive(Clone, PartialEq, Eq, Debug)]
// struct Error<'a> {
// s: &'a str,
// pos: usize,
// kind: ErrorKind,
// }
//
// impl<'a> std::fmt::Display for Error<'a> {
// fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
// let Error { pos, kind, s } = self;
//
// write!(f, "regex error in \"{}\" at position {}: {}", s, pos, kind)
// }
// }
// #[test]
// fn test_regex() {
// let input = r"^ENWWW(NEEE|SSE(EE|N))$";
// assert_eq!(Regex::as_str(&input.parse().unwrap()), input);
// }
//

// }
// }
// impl <'a> FromStr for Regex<'a> {
// type Err = Error<'a>;
//
// fn from_str(s: & str) -> Result<Self, Error<'a>> {
// let s = s.to_owned();
// if s.starts_with('^') && s.ends_with('$') {
// Ok(Regex::Start(Box::new(Regex::parse_from_tokens(&s[1..s.len() - 1],
// false)?))) } else {
// Ok(Regex::parse_from_tokens(&s, false)?)
// }
// }
// }
//
// fn branch_group(s: &str, start: usize) -> Result<&str, Error> {
// let mut depth = 0;
// for (i, b) in s[start..].bytes().enumerate() {
// match b {
// b'(' => depth +=1,
// b')' if depth == 0 => return Err(Error{s, pos: i+start, kind:
// ErrorKind::MismatchedRightParen}), b')' if depth == 1 => return
// Ok(&s[start+1..start+i]), b')' => depth -= 1,
// _ => {},
// }
// }
// Err(Error{s, pos: s.len()-start, kind:ErrorKind::HangingLeftParen})
// }
//
// impl <'a> Regex<'a> {
// fn parse_from_tokens(s: &str)-> Result<Regex, Error> {
//
// let mut parens = Vec::new();
// let mut chain = Vec::new();
// let mut it = s.bytes().enumerate();
// loop {
// while parens.len() > 0 {
// match it.next() {
// None => return Err(Error{s, pos: parens.pop().unwrap(), kind:
// ErrorKind::HangingLeftParen }), Some((pos, b'('))=>
// chain.push(Regex::Branch(Regex::parse_from_tokens(branch_group(s, pos)))), }
// }
// while parens.len() == 0 {
// let (pos, b) = it.next().unwrap();
// match b {
// b'(' =>  {
// chain.push(Regex::Single(&s[0..pos]));parens.push(pos);}
// b')' => return Err(Error {
// s,
// pos,
// kind: ErrorKind::MismatchedRightParen,
// }),
//
//
// b'|' =>
// return Err(Error {
// s,
// pos,
// kind: ErrorKind::BranchOutsideGroup,
// }),
// b'N' | b'E' | b'S' | b'W' => {},
// b => return Err(Error{s, pos, kind: ErrorKind::UnknownByte(b)})
// };
// }
//
//
// }
// while depth == 0 {
//
// }
// let depth = parens.len();
// if depth == 0 {
//
// } else {
// match token {
// we are no longer in a gruop
// (b')' => {let start = parens.pop().unwrap();
// subslices.insert(s[start..pos],
// Regex::Branch(parse_from_tokens(&s[start+1..pos-1])?));
//
// },
// if it is a branching group, we merely count the nesting:
// (b'N' | b'E' | b'S' | b'W' | b'|') => {},
//
// (b'(', _) => parens.push(pos),
// (b')', _) => {
// parens.pop();
// },
// (b, _) => {
// return Err(Error {
// s: s.to_string(),
// pos,
// kind: ErrorKind::UnknownByte(b),
// });
// },
// }
// }
//
// if let Some(pos) = parens.pop() {
// return Err(Error {
// s: s.to_string(),
// pos,
// kind: ErrorKind::HangingLeftParen,
// });
// }
//
// Ok(match chain.len() {
// 0 => Regex::Empty,
// 1 => chain.into_iter().next().unwrap(),
// n => Regex::Chain(chain),
// })
// }
//
// fn parse_branch(s: &str, depth: usize) -> Result<(usize, Regex), Error> {
// let mut branches = Vec::new();
// let mut last_branch = 0;
// for (i, token) in s.bytes().enumerate() {
// match token {
// b'|' => branches.push(Regex::parse_from_tokens(&s[last_branch..i])?);
// last_branch = i + 1;
// b'(' => branches.push(parse_branch(&s[i+1])
// };
// }
// if last_branch == s.len() {
// branches.push(Regex::Empty);
// }
// Ok(Regex::Branch(branches))
// }
// }
//
// impl Direction {
// fn to_char(self) -> char { self.into() }
// }
//
// impl Into<char> for Direction {
// fn into(self) -> char {
// match self {
// Direction::North => 'N',
// Direction::South => 'S',
// Direction::East => 'E',
// Direction::West => 'W',
// }
// }
// }
//
// impl TryFrom<char> for Direction {
// type Error = char;
//
// fn try_from(c: char) -> Result<Self, char> {
// Ok(match c {
// 'N' => Direction::North,
// 'S' => Direction::South,
// 'E' => Direction::East,
// 'W' => Direction::West,
// _ => return Err(c),
// })
// }
// }
//
// impl std::fmt::Display for ErrorKind {
// fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
// match self {
// ErrorKind::BranchOutsideGroup => write!(f, "branch outside group"),
// ErrorKind::HangingLeftParen => write!(f, "hanging left parentheses"),
// ErrorKind::MismatchedRightParen => write!(f, "mismatched right parentheses"),
// ErrorKind::UnknownByte(c) => write!(f, "unknown character {}", *c as char),
// }
// }
// }
//
