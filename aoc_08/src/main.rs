fn main() {
    println!("aoc_2018_08_a: {}", a(INPUT));
}

use std::cmp::Ordering;
use std::collections::BTreeMap;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Default)]
struct Key {
    depth: usize,
    i: usize,
}

#[derive(Clone, Debug, Default)]
struct Val {
    metadata: Vec<usize>,
    children: Vec<usize>,
}

impl Val {
    fn allocate(it: &mut Iterator<Item = usize>) -> Option<Self> {
        if let (Some(c), Some(m)) = (it.next(), it.next()) {
            Some(Val {
                metadata: Vec::with_capacity(m),
                children: Vec::with_capacity(c),
            })
        } else {
            None
        }
    }

    fn take_metadata(&mut self, it: &mut Iterator<Item = usize>) -> usize {
        self.metadata.extend(it.take(self.metadata.capacity()));
        self.metadata.len()
    }

    fn add_child(&mut self, it: &mut Iterator<Item = usize>) {
        self.children.push(it.next().unwrap())
    }

    fn has_all_children(&self) -> bool {
        self.children.len() == self.children.capacity()
    }
}

type Tree = std::collections::BTreeMap<Key, Val>;

const INPUT: &str = include_str!("a.in");

fn a(input: &str) -> u32 {
    let mut nodes = input
        .split_whitespace()
        .filter_map(|s| s.parse::<u32>().ok());

    let mut stack = Vec::<(u32, u32)>::new();

    stack.push((nodes.next().unwrap(), nodes.next().unwrap()));
    let mut metadata_sum = 0;
    while let Some((children, metadata)) = stack.pop() {
        match (children, metadata) {
            (0, 0) => continue,
            (0, metadata) => {
                metadata_sum += nodes.next().unwrap();
                stack.push((0, metadata - 1));
            }
            (children, metadata) => {
                stack.push((children - 1, metadata));
                stack.push((nodes.next().unwrap(), nodes.next().unwrap()));
            }
        }
    }
    metadata_sum
}

///--- Part Two ---
///The second check is slightly more complicated: you need to find the value of the root node (A in the example above).
///
///The value of a node depends on whether it has child nodes.
///
///If a node has no child nodes, its value is the sum of its metadata entries. So, the value of node B is 10+11+12=33, and the value of node D is 99.
///
///However, if a node does have child nodes, the metadata entries become indexes which refer to those child nodes. A metadata entry of 1 refers to the first child node, 2 to the second, 3 to the third, and so on. The value of this node is the sum of the values of the child nodes referenced by the metadata entries. If a referenced child node does not exist, that reference is skipped. A child node can be referenced multiple time and counts each time it is referenced. A metadata entry of 0 does not refer to any child node.
///
///For example, again using the above nodes:
///
///Node C has one metadata entry, 2. Because node C has only one child node, 2 references a child node which does not exist, and so the value of node C is 0.
///Node A has three metadata entries: 1, 1, and 2. The 1 references node A's first child node, B, and the 2 references node A's second child node, C. Because node B has a value of 33 and node C has a value of 0, the value of node A is 33+33+0=66.
///So, in this example, the value of the root node is 66.
///What is the value of the root node?
fn b(input: &str) -> usize {
    let mut it = input
        .split_whitespace()
        .filter_map(|s| s.parse::<usize>().ok());

    let key = Key::default();
    let mut stack = vec![key.clone()];
    let mut tree = Tree::new();
    tree.insert(key.clone(), Val::allocate(&mut it).unwrap());
    let mut i = 2;
    loop {
        let key = stack.pop().unwrap();
        match tree.get_mut(&key).unwrap() {
            ref mut val if val.has_all_children() => i += val.take_metadata(&mut it),
            val => val.add_child(&mut it),
        };

        let new_key = Key {
            i,
            depth: key.depth + 1,
        };

        stack.push(key);
        stack.push(new_key.clone());
        if let Some(val) = Val::allocate(&mut it) {
            tree.insert(new_key, val);
            i += 2;
        } else {
            break;
        }
    }
    eprintln!("tree: {:#?}", tree);
    let mut stored_vals: BTreeMap<usize, usize> = BTreeMap::new();

    for (Key { i, depth }, Val { children, metadata }) in tree.into_iter().rev() {
        if children.is_empty() {
            stored_vals.insert(i, metadata.iter().cloned().sum());
        } else {
            stored_vals.insert(
                i,
                metadata
                    .into_iter()
                    .filter_map(|j| children.get(j - 1))
                    .map(|child| stored_vals[child])
                    .sum::<usize>(),
            );
        }
        eprintln!("{}, {}, {:#?}", i, depth, stored_vals);
    }
    stored_vals[&0]
}
#[test]
fn test_a() {
    let input = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2";
    assert_eq!(a(input), 138)
}

#[test]
fn test_b() {
    let input = "2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2";

    assert_eq!(b(input), 66);
}
/*



--- Day 8: Memory Maneuver ---
The sleigh is much easier to pull than you'd expect for something its weight. Unfortunately, neither you nor the Elves know which way the North Pole is from here.

You check your wrist device for anything that might help. It seems to have some kind of navigation system! Activating the navigation system produces more bad news: "Failed to start navigation system. Could not read software license file."

The navigation system's license file consists of a list of numbers (your puzzle input). The numbers define a data structure which, when processed, produces some kind of tree that can be used to calculate the license number.

The tree is made up of nodes; a single, outermost node forms the tree's root, and it contains all other nodes in the tree (or contains nodes that contain nodes, and so on).

Specifically, a node consists of:

A header, which is always exactly two numbers:
The quantity of child nodes.
The quantity of metadata entries.
Zero or more child nodes (as specified in the header).
One or more metadata entries (as specified in the header).
Each child node is itself a node that has its own header, child nodes, and metadata. For example:

2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2
A----------------------------------
    B----------- C-----------
                     D-----
In this example, each node of the tree is also marked with an underline starting with a letter for easier identification. In it, there are four nodes:

A, which has 2 child nodes (B, C) and 3 metadata entries (1, 1, 2).
B, which has 0 child nodes and 3 metadata entries (10, 11, 12).
C, which has 1 child node (D) and 1 metadata entry (2).
D, which has 0 child nodes and 1 metadata entry (99).
The first check done on the license file is to simply add up all of the metadata entries. In this example, that sum is 1+1+2+10+11+12+2+99=138.

What is the sum of all metadata entries?
*/
