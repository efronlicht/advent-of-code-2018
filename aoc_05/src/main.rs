extern crate gapbuffer;
use gapbuffer::GapBuffer;
fn main() {
    println!("AOC_2018_a => {}", a(INPUT.bytes()));
    println!("AOC_2018_b => {}", b(INPUT));
}

const INPUT: &str = include_str!("a.in");

fn a(input: impl Iterator<Item = u8>) -> usize {
    let mut buf: GapBuffer<u8> = input.collect();
    let mut any_deleted = true;
    while any_deleted {
        any_deleted = false;
        let mut i = 0;
        while let (Some(a), Some(b)) = (buf.get(i), buf.get(i + 1)) {
            i = match (a, b) {
                (b'a'..=b'z', b'A'..=b'Z') | (b'A'..=b'Z', b'a'..=b'z')
                    if a.to_ascii_lowercase() == b.to_ascii_lowercase() =>
                {
                    any_deleted = true;
                    buf.remove(i);
                    buf.remove(i);
                    if i > 0 {
                        i - 1
                    } else {
                        break;
                    }
                }
                _ => i + 1,
            }
        }
    }
    buf.len()
}

fn b(input: &str) -> usize {
    (b'a'..=b'z')
        .map(|letter| a(input.bytes().filter(|b| b.to_ascii_lowercase() != letter)))
        .min()
        .unwrap()
}

#[test]
fn test_b() {
    assert_eq!(b("dabAcCaCBAcCcaDA"), "daDA".len())
}
#[test]
fn test_a() {
    assert_eq!(a("dabAcCaCBAcCcaDA".bytes()), "dabCBAcaDA".len());
    assert_eq!(a("abBA".bytes()), "".len());
}
