use std::collections::HashMap;
fn main() {
    println!("aoc_2018_18_a => {},", a(INPUT));
    println!("aoc_2018_18_b => {},", b(INPUT));
}

const OPEN: char = '.';
const TREES: char = '|';
const LUMBERYARD: char = '#';
const TREE_BITPATTERN: u128 = 0b10;
const YARD_BITPATTERN: u128 = 0b01;
const OPEN_BITPATTERN: u128 = 0;

type Grid = [[Acre; 50]; 50];
#[derive(Copy, Clone, Debug, PartialEq, Eq)]
enum Acre {
    Open,
    Trees,
    Lumberyard,
}

const INPUT: &str = include_str!("a.in");

fn a(input: &str) -> usize {
    let mut grid = parse_input(input);
    for _ in 0..10 {
        grid = run_lumberyard(grid);
    }
    get_score(&grid)
}
fn b(input: &str) -> usize {
    const MAX_ITER: usize = 1_000_000_000;
    let mut grid = parse_input(input);
    let mut seen: HashMap<Vec<u128>, usize> = HashMap::new();

    // find start of cycle
    let mut i = 0;
    let cycle_len = loop {
        let hash: Vec<u128> = grid.iter().map(to_linehash).collect();
        match seen.insert(hash, i) {
            Some(j) => break i - j,
            None => {
                grid = run_lumberyard(grid);
                i += 1
            }
        }
    };
    // fast_forward:
    let remaining = (MAX_ITER - i) % cycle_len;

    for _ in 0..remaining {
        grid = run_lumberyard(grid)
    }
    get_score(&grid)
}

fn parse_input(input: &str) -> [[Acre; 50]; 50] {
    let mut grid = [[Acre::Open; 50]; 50];
    let rows = input
        .lines()
        .filter(|line| !line.is_empty() && !line.chars().all(|c| c.is_ascii_whitespace()))
        .map(str::trim);
    for (y, row) in (0..).zip(rows) {
        for (x, c) in (0..).zip(row.chars()) {
            grid[y][x] = Acre::from_char(c);
        }
    }
    grid
}

#[inline]
fn run_lumberyard(grid: Grid) -> Grid {
    let mut next = [[Acre::Open; 50]; 50];
    for y in 0..50 {
        for x in 0..50 {
            let (_, trees, lumberyards) = neighbors(&grid, y, x);
            next[y][x] = match grid[y][x] {
                Acre::Open if trees > 2 => Acre::Trees,
                Acre::Trees if lumberyards > 2 => Acre::Lumberyard,
                Acre::Lumberyard if trees > 0 && lumberyards > 0 => Acre::Lumberyard,
                Acre::Lumberyard => Acre::Open,
                Acre::Open => Acre::Open,
                Acre::Trees => Acre::Trees,
            }
        }
    }

    next
}
#[inline]
// this is unnecessary and I could have just used a Vec<Vec<Acre>> for the hash, but it's kinda neat!
fn to_linehash(line: &[Acre; 50]) -> u128 {
    let mut hash = 0;
    for (i, acre) in line.iter().enumerate() {
        let offset = i * 2;
        hash |= match acre {
            Acre::Open => OPEN_BITPATTERN,
            Acre::Lumberyard => YARD_BITPATTERN << offset,
            Acre::Trees => TREE_BITPATTERN << offset,
        }
    }
    hash
}

fn get_score(grid: &Grid) -> usize {
    let acres = grid.iter().flat_map(|row| row.iter());

    let yards = acres
        .clone()
        .filter(|acre| **acre == Acre::Lumberyard)
        .count();
    let trees = acres.filter(|acre| **acre == Acre::Trees).count();
    yards * trees
}

fn neighbors(grid: &Grid, y: usize, x: usize) -> (usize, usize, usize) {
    let (y, x) = (y as isize, x as isize);
    let (mut open, mut trees, mut lumberyards) = (0, 0, 0);
    for dy in (-1..=1).filter(|dy| y + dy < 50 && y + dy >= 0) {
        for dx in (-1..=1).filter(|dx| x + dx < 50 && x + dx >= 0) {
            if !(dx == 0 && dy == 0) {
                let (y, x) = ((y + dy) as usize, (x + dx) as usize);
                match grid[y][x] {
                    Acre::Lumberyard => lumberyards += 1,
                    Acre::Open => open += 1,
                    Acre::Trees => trees += 1,
                }
            }
        }
    }
    (open, trees, lumberyards)
}

impl Acre {
    fn from_char(c: char) -> Self {
        match c {
            OPEN => Acre::Open,
            TREES => Acre::Trees,
            LUMBERYARD => Acre::Lumberyard,
            _ => panic!("bad input"),
        }
    }
}

impl Into<char> for Acre {
    fn into(self) -> char {
        match self {
            Acre::Open => OPEN,
            Acre::Trees => TREES,
            Acre::Lumberyard => LUMBERYARD,
        }
    }
}
