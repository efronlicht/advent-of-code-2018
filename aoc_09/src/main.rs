use gapbuffer::GapBuffer;
use std::time::Instant;
const LAST_MARBLE_SCORE: usize = 72170;
const PLAYERS: usize = 470;

fn print_elapsed(t: Instant) {
    let elapsed = t.elapsed();
    println!(
        "ran in {}.{:03}s",
        elapsed.as_secs(),
        elapsed.subsec_millis()
    );
}
fn main() {
    let a_start = Instant::now();
    println!("aoc_2018_09_a => {}", a(PLAYERS, LAST_MARBLE_SCORE));
    print_elapsed(a_start);

    let b_start = Instant::now();
    println!("aoc_2018_09_b => {}", a(PLAYERS, LAST_MARBLE_SCORE * 100));
    print_elapsed(b_start)
}

fn a(players: usize, last_marble: usize) -> usize {
    let mut pos = 0;
    let mut marbles = GapBuffer::with_capacity(LAST_MARBLE_SCORE);
    let mut scores = vec![0; PLAYERS];
    marbles.insert(0, 0);
    marbles.insert(1, 1);

    for (player, marble) in (2..).map(|i| i % PLAYERS).zip(2..=last_marble) {
        if marble % 23 == 0 {
            pos = if pos < 7 {
                marbles.len() - 7 + pos
            } else {
                pos - 7
            };
            let removed = marbles.remove(pos).unwrap();
            scores[player] += marble + removed;
        } else {
            pos = (pos + 2) % marbles.len();
            marbles.insert(pos, marble);
        }
    }
    scores.into_iter().max().unwrap()
}

#[test]
fn test_a() {
    assert_eq!(a(9, 25), 32);
}
