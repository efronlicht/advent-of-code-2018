fn main() {
    println!("aoc_2018_19_a => {}", a(INPUT));
}

const INPUT: &str = include_str!("program.in");
#[derive(Clone, PartialEq, Eq, Debug, Default)]
struct Registers([u32; 6]);
type Args = (u32, u32, u32);

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
struct Instruction {
    op: OpCode,
    args: Args,
}

#[derive(Clone, PartialEq, Eq, Debug)]
struct CPU {
    registers: Registers,
    ip_register: u32,
    ip: u32,
    instructions: Vec<Instruction>,
}
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub enum OpCode {
    AddR,
    AddI,

    MulR,
    MulI,

    BAnR,
    BAnI,

    BOrR,
    BOrI,

    SetR,
    SetI,

    GtIR,
    GtRI,
    GtRR,

    EqIR,
    EqRI,
    EqRR,
}
#[test]
fn test_a() {
    let program = "#ip 0
seti 5 0 1
seti 6 0 2
addi 0 1 0
addr 1 2 3
setr 1 0 0
seti 8 0 4
seti 9 0 5";

    assert_eq!(a(program), 6);
}
impl Instruction {
    fn parse(line: &str) -> Option<Self> {
        let mut split = line.split_whitespace();
        Some(Instruction {
            op: OpCode::from_name(split.next()?)?,
            args: (
                split.next()?.parse().unwrap(),
                split.next()?.parse().unwrap(),
                split.next()?.parse().unwrap(),
            ),
        })
    }
}
///What value is left in register 0 when the background process halts?
fn a(s: &str) -> u32 {
    let mut cpu = CPU::new(s);
    dbg!(&cpu);
    let mut tick = 0;
    while cpu.exec_next() {
        tick += 1;
        dbg!(tick);
        dbg!(&cpu.registers);
    }
    cpu.registers[0]
}
impl CPU {
    fn new(s: &str) -> Self {
        let mut lines = s.lines();
        CPU {
            ip: 0,
            ip_register: lines
                .next()
                .unwrap()
                .chars()
                .filter(|c| c.is_numeric())
                .collect::<String>()
                .parse::<u32>()
                .unwrap(),
            registers: Registers::default(),
            instructions: lines.filter_map(|line| Instruction::parse(line)).collect(),
        }
    }

    fn current_instruction(&self) -> Option<Instruction> {
        self.instructions
            .get(self.ip as usize)
            .and_then(|ins| Some(*ins))
    }

    ///When the instruction pointer is bound to a register, its value is written to that register just before each instruction is executed,
    /// and the value of that register is written back to the instruction pointer immediately after each instruction finishes execution.
    ///  Afterward, move to the next instruction by adding one to the instruction pointer, even if the value in the instruction pointer
    /// was just updated by an instruction
    /// (Because of this, instructions must effectively set the instruction pointer to the instruction before the one they want executed next.)

    fn exec_next(&mut self) -> bool {
        let current_instruction = if let Some(ins) = self.instructions.get(self.ip as usize) {
            *ins
        } else {
            return false;
        };

        self.registers[self.ip_register] = self.ip;

        self.registers.exec(current_instruction);
        self.ip = self.registers[self.ip_register];
        self.ip += 1;
        true
    }
}

#[test]
fn test_cpu_new() {
    let want = CPU {
        ip: 0,
        ip_register: 5,
        registers: Registers::default(),
        instructions: vec![
            Instruction {
                op: AddI,
                args: (5, 16, 5),
            },
            Instruction {
                op: SetI,
                args: (1, 7, 3),
            },
        ],
    };
    assert_eq!(CPU::new("#ip 5\naddi 5 16 5\nseti 1 7 3"), want)
}

impl Registers {
    pub fn exec(&mut self, instruction: Instruction) {
        let Instruction { op, args } = instruction;
        match op {
            OpCode::AddI => self.add_i(args),
            OpCode::AddR => self.add_r(args),
            OpCode::MulR => self.mul_r(args),
            OpCode::MulI => self.mul_i(args),
            OpCode::BAnI => self.bitwise_and_i(args),
            OpCode::BAnR => self.bitwise_and_r(args),
            OpCode::BOrI => self.bitwise_or_i(args),
            OpCode::BOrR => self.bitwise_or_r(args),
            OpCode::SetI => self.set_i(args),
            OpCode::SetR => self.set_r(args),
            OpCode::GtIR => self.gtir(args),
            OpCode::GtRI => self.gtri(args),
            OpCode::GtRR => self.gtrr(args),
            OpCode::EqIR => self.eqir(args),
            OpCode::EqRI => self.eqri(args),
            OpCode::EqRR => self.eqrr(args),
        }
    }

    /// addr (add register) stores into register C the result of adding register A and register B.
    fn add_r(&mut self, (a, b, c): Args) {
        self[c] = self[a] + self[b];
    }
    /// addi (add immediate) stores into register C the result of adding register A and value B.
    fn add_i(&mut self, (a, b, c): Args) {
        self[c] = self[a] + b;
    }
    /// mulr (multiply register) stores into register C the result of multiplying register A and register B.
    fn mul_r(&mut self, (a, b, c): Args) {
        self[c] = self[a] * self[b];
    }
    /// muli (multiply immediate) stores into register C the result of multiplying register A and value B.
    fn mul_i(&mut self, (a, b, c): Args) {
        self[c] = self[a] * b
    }

    /// banr (bitwise AND register) stores into register C the result of the bitwise AND of register A and register B.
    fn bitwise_and_r(&mut self, (a, b, c): Args) {
        self[c] = self[a] & self[b]
    }
    /// bani (bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
    fn bitwise_and_i(&mut self, (a, b, c): Args) {
        self[c] = self[a] & b;
    }
    /// borr (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
    fn bitwise_or_r(&mut self, (a, b, c): Args) {
        self[c] = self[a] | self[b];
    }
    /// bori (bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
    fn bitwise_or_i(&mut self, (a, b, c): Args) {
        self[c] = self[a] | b;
    }

    /// setr (set register) copies the contents of register A into register C. (Input B is ignored.)
    fn set_r(&mut self, (a, _, c): Args) {
        self[c] = self[a];
    }
    /// seti (set immediate) stores value A into register C. (Input B is ignored.)
    fn set_i(&mut self, (a, _, c): Args) {
        self[c] = a;
    }
    ///gtir (greater-than immediate/register) sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
    fn gtir(&mut self, (a, b, c): Args) {
        self[c] = if a > self[b] { 1 } else { 0 };
    }
    ///gtri (greater-than register/immediate) sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.

    fn gtri(&mut self, (a, b, c): Args) {
        self[c] = if self[a] > b { 1 } else { 0 };
    }
    ///gtrr (greater-than register/register) sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.

    fn gtrr(&mut self, (a, b, c): Args) {
        self[c] = if self[a] > self[b] { 1 } else { 0 };
    }
    ///eqir (equal immediate/register) sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0

    fn eqir(&mut self, (a, b, c): Args) {
        self[c] = if a == self[b] { 1 } else { 0 };
    }

    ///eqri (equal register/immediate) sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
    fn eqri(&mut self, (a, b, c): Args) {
        self[c] = if self[a] == b { 1 } else { 0 };
    }

    ///eqrr (equal register/register) sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
    fn eqrr(&mut self, (a, b, c): Args) {
        self[c] = if self[a] == self[b] { 1 } else { 0 };
    }
}

impl std::ops::Index<u32> for Registers {
    type Output = u32;
    fn index(&self, i: u32) -> &u32 {
        &self.0[i as usize]
    }
}

impl std::ops::IndexMut<u32> for Registers {
    fn index_mut(&mut self, i: u32) -> &mut u32 {
        &mut self.0[i as usize]
    }
}

impl Into<&'static str> for OpCode {
    fn into(self) -> &'static str {
        match self {
            AddR => "addr",
            AddI => "addi",
            MulR => "mulr",
            MulI => "muli",
            BAnR => "banr",
            BAnI => "bani",
            BOrR => "borr",
            BOrI => "bori",
            SetR => "setr",
            SetI => "seti",
            EqIR => "eqir",
            EQRI => "eqri",
            EQRR => "eqrr",
        }
    }
}

use self::OpCode::*;

impl OpCode {
    pub const OPCODES: [OpCode; 16] = [
        AddR, AddI, MulR, MulI, BAnR, BAnI, BOrR, BOrI, SetR, SetI, GtIR, GtRI, GtRR, EqIR, EqRI,
        EqRR,
    ];
    pub fn from_name(s: &str) -> Option<Self> {
        Some(match s {
            "addr" => AddR,
            "addi" => AddI,
            "mulr" => MulR,
            "muli" => MulI,
            "banr" => BAnR,
            "bani" => BAnI,
            "borr" => BOrR,
            "bori" => BOrI,
            "setr" => SetR,
            "seti" => SetI,
            "eqir" => EqIR,
            "eqri" => EqRI,
            "eqrr" => EqRR,
            _ => return None,
        })
    }

    pub fn iter() -> impl Iterator<Item = OpCode> {
        Self::OPCODES.iter().cloned()
    }
}
