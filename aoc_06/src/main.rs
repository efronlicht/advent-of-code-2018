use std::cmp::Ordering;
use std::cmp::{max, min};
use std::collections::BTreeMap;
use std::str::FromStr;

fn main() {
    let points: Vec<Point> = INPUT
        .lines()
        .filter_map(|line| line.parse::<Point>().ok())
        .collect();
    let bounds: BoundingBox = points.iter().cloned().collect();
    println!("aoc_2018_06_a: {}", part1(bounds, &points));
    println!("aoc_018_06_b: {}", part2(bounds, &points));
}

const INPUT: &str = include_str!("a.in");
#[derive(Copy, Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
struct Point {
    x: u16,
    y: u16,
}
#[derive(Clone, Debug, PartialEq, Eq)]
struct PointIter {
    p: Option<Point>,
    bounds: BoundingBox,
}

#[derive(Copy, Clone, PartialEq, Debug, Eq)]
struct BoundingBox {
    top_left: Point,
    bottom_right: Point,
}

impl Point {
    fn distance(self, other: Self) -> i32 {
        let dx = i32::abs(self.x as i32 - other.x as i32);
        let dy = i32::abs(self.y as i32 - other.y as i32);
        dx + dy
    }

    fn closest(self, points: &[Point]) -> Option<Point> {
        let dists: Vec<_> = points.iter().cloned().map(|p| self.distance(p)).collect();
        let (min_dist_point, dist) = Iterator::zip(points.iter().cloned(), &dists)
            .min_by_key(|(_, d)| *d)
            .unwrap();
        if dists.iter().filter(|d| *d == dist).count() > 1 {
            None
        } else {
            Some(min_dist_point)
        }
    }
}
impl std::iter::FromIterator<Point> for BoundingBox {
    fn from_iter<I: IntoIterator<Item = Point>>(it: I) -> BoundingBox {
        let (top_left, bottom_right) = it.into_iter().fold(
            (
                Point {
                    x: u16::max_value(),
                    y: u16::max_value(),
                },
                Point { x: 0, y: 0 },
            ),
            |(top_left, bottom_right), p| (min(p, top_left), max(p, bottom_right)),
        );
        BoundingBox {
            top_left,
            bottom_right,
        }
    }
}

impl BoundingBox {
    fn on_edge(self, p: Point) -> bool {
        p.x == self.top_left.x
            || p.y == self.top_left.y
            || p.x == self.bottom_right.x
            || p.y == self.bottom_right.y
    }
}

fn part1(bounds: BoundingBox, points: &[Point]) -> u32 {
    let mut dists = BTreeMap::<Point, u32>::new();
    for p in bounds
        .into_iter()
        .filter_map(|p: Point| p.closest(points))
        .filter(|p| !bounds.on_edge(*p))
    {
        *dists.entry(p).or_default() += 1;
    }

    dists.values().cloned().max().unwrap()
}

#[cfg(test)]
const TEST_INPUT: [Point; 6] = [
    Point { x: 1, y: 1 },
    Point { x: 1, y: 6 },
    Point { x: 8, y: 3 },
    Point { x: 3, y: 4 },
    Point { x: 5, y: 5 },
    Point { x: 8, y: 9 },
];
#[test]

fn test_part_1() {
    let bounds = TEST_INPUT.iter().cloned().collect::<BoundingBox>();
    assert_eq!(part1(bounds, &TEST_INPUT), 17)
}

fn part2(bounds: BoundingBox, points: &[Point]) -> usize {
    bounds
        .into_iter()
        .filter(|p| points.iter().fold(0, |acc, q| acc + p.distance(*q)) < 10_000)
        .count()
}
#[test]
fn test_part_2() {
    let bounds = TEST_INPUT.iter().cloned().collect::<BoundingBox>();
    assert_eq!(part2(bounds, &TEST_INPUT), 5);
}

impl IntoIterator for BoundingBox {
    type IntoIter = PointIter;
    type Item = Point;
    fn into_iter(self) -> Self::IntoIter {
        PointIter {
            p: Some(self.top_left),
            bounds: self,
        }
    }
}
impl Iterator for PointIter {
    type Item = Point;
    fn next(&mut self) -> Option<Self::Item> {
        let mut ret = match self.p {
            None => None,
            Some(ref p) if *p == self.bounds.bottom_right => None,
            Some(Point { x, y }) if x == self.bounds.bottom_right.x => Some(Point {
                x: self.bounds.top_left.x,
                y: y + 1,
            }),
            Some(Point { x, y }) => Some(Point { x: x + 1, y }),
        };
        std::mem::swap(&mut self.p, &mut ret);
        ret
    }
}

#[cfg(test)]
impl BoundingBox {
    fn width(&self) -> u32 {
        (self.bottom_right.x - self.top_left.x + 1) as u32
    }

    fn height(&self) -> u32 {
        (self.bottom_right.y - self.top_left.y + 1) as u32
    }
}

impl<N: Into<u16>> From<(N, N)> for Point {
    fn from((x, y): (N, N)) -> Self {
        Point {
            x: x.into(),
            y: y.into(),
        }
    }
}

#[test]
fn test_bb_iter() {
    let bb = BoundingBox {
        top_left: Point { x: 1, y: 1 },
        bottom_right: Point { x: 5, y: 1 },
    };
    assert_eq!(bb.width(), 5);
    assert_eq!(bb.height(), 1);

    assert_eq!(
        bb.into_iter().collect::<Vec<_>>(),
        (1..6).map(|x| Point { x, y: 1 }).collect::<Vec<Point>>()
    );
}

#[test]
fn test_point_dist() {
    let tt = vec![
        (Point { x: 0, y: 1 }, Point::default(), 1),
        (Point { x: 5, y: 0 }, Point::default(), 5),
        (Point { x: 5, y: 1 }, Point::default(), 6),
        (Point { x: 5, y: 5 }, Point { x: 5, y: 5 }, 0),
        (Point { x: 5, y: 5 }, Point { x: 5, y: 0 }, 5),
        (Point { x: 5, y: 5 }, Point { x: 5, y: 3 }, 2),
        (Point { x: 5, y: 5 }, Point { x: 3, y: 5 }, 2),
        (Point { x: 5, y: 5 }, Point { x: 0, y: 5 }, 5),
        (Point { x: 8, y: 3 }, Point { x: 5, y: 3 }, 3),
    ];
    for (p, q, want) in tt {
        assert_eq!(
            Point::distance(p, q),
            want,
            "Point::distance({:?},{:?} should be {}), but was {}",
            p,
            q,
            want,
            Point::distance(p, q)
        );
    }
}

impl FromStr for Point {
    type Err = std::num::ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split(",").map(str::trim);
        match (split.next(), split.next()) {
            (Some(x), Some(y)) => Ok(Point {
                x: x.parse()?,
                y: y.parse()?,
            }),
            _ => Err(s.parse::<u32>().unwrap_err()),
        }
    }
}

/// this is pointless, but it's a cool example of an actual partial order
/// one box is greater than another if it totally contains the other
impl PartialOrd for BoundingBox {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (
            self.top_left.cmp(&other.top_left),
            self.bottom_right.cmp(&other.bottom_right),
        ) {
            (a, b) if a == b => Some(a),
            _ => None,
        }
    }
}
