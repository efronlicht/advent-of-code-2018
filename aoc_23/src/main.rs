fn main() {
    println!("aoc_2018_23_a => {}", a(INPUT));
}
use std::collections::{BTreeMap, BTreeSet, HashSet};
use std::hash::Hash;
type IRange = std::ops::RangeInclusive<i32>;

type Point = (i32, i32, i32);
#[derive(Clone, Copy, PartialEq, Eq, Hash, Debug)]
struct Bot {
    pos: Point,
    r: i32,
}

fn get_max(input: &str) -> i32 {
    input
        .lines()
        .map(|line| max_abs_elem(line.parse().unwrap()))
        .max()
        .unwrap()
}

fn max_abs_elem(b: Bot) -> i32 {
    let Bot { pos: (x, y, z), r } = b;
    let (x, y, z) = (x.abs(), y.abs(), z.abs());
    x.max(y).max(z).max(r as i32)
}

use std::borrow::Cow;
type Intersection = BTreeSet<Bot>;

const INPUT: &str = include_str!("bots.in");

use std::num::ParseIntError;
use std::str::FromStr;

fn a(input: &str) -> usize {
    let bots: Vec<Bot> = input.lines().map(|line| line.parse().unwrap()).collect();
    let largest_radius_bot = *bots.iter().max_by_key(|b| b.r).unwrap();
    bots.into_iter()
        .filter(|b| largest_radius_bot.in_radius(b))
        .count()
}

fn b(input: &str) {
    let mut intersections = Bot::intersections(input.lines().map(|line| line.parse().unwrap()));

    let mut longest_path = 1;
    let mut best_chains: BTreeMap<Bot, Vec<Bot>> = BTreeMap::new();

    'outer: while intersections.len() > longest_path {
        let mut keys: Vec<Bot> = intersections.keys().cloned().collect();
        keys.sort_by_key(|k| intersections[k].len());
        keys.reverse();

        for (mut i, k) in keys.iter().enumerate() {
            let mut connections: Vec<BTreeSet<_>> = vec![intersections[k].clone()];

            let keys: Vec<&Bot> = keys.iter().filter(|k| connections[0].contains(k)).collect();

            let k = *k;
            let mut chain: Vec<Bot> = Vec::new();
            let mut avoid = BTreeSet::new();
            avoid.insert(k);
            let empty = BTreeSet::default();
            let mut prev = &empty;
            while let Some((j, r)) = keys[i..]
                .iter()
                .enumerate()
                .find(|(_, r)| prev.contains(**r))
            {
                i += j;
                let new = prev.intersection(&intersections[r]).cloned().collect();

                connections.push(new);
                prev = &connections[connections.len() - 1];
                chain.push(k);
            }
            if chain.len() > longest_path {
                longest_path = chain.len();
                intersections = prune_length(intersections, longest_path);
                continue 'outer;
            }
            let best_chain = best_chains.entry(k).or_default();
            if chain.len() > best_chain.len() {
                *best_chain = chain.clone();
            }
            if let Some(k) = chain.pop() {
                avoid.insert(k);
            }
            // we have totally exhausted this key;
            if avoid.len() == intersections[&k].len() {
                intersections = prune_elem(intersections, k)
            }
        }
    }
}

/// remove all entries where the total number of intersections is shorter than the current known longest path
fn prune_length(mut intersections: Intersections, longest_path: usize) -> Intersections {
    let to_remove: Vec<Bot> = intersections
        .iter()
        .filter_map(|(k, v)| {
            if v.len() < longest_path {
                Some(*k)
            } else {
                None
            }
        })
        .collect();
    for k in to_remove {
        intersections = prune_elem(intersections, k);
    }
    intersections
}

#[inline]
fn prune_elem(mut intersections: Intersections, bot: Bot) -> Intersections {
    if let Some(connections) = intersections.remove(&bot) {
        for c in connections {
            if let Some(v) = intersections.get_mut(&c) {
                v.remove(&bot);
            }
        }
    }
    intersections
}
type Intersections = BTreeMap<Bot, BTreeSet<Bot>>;

fn remove_from_all(mut intersections: Intersections, bot: Bot) -> Intersections {
    let edges: Vec<Bot> = intersections[&bot].iter().cloned().collect();
    for edge in edges {
        intersections.get_mut(&edge).unwrap().remove(&bot);
    }
    intersections.remove(&bot);
    intersections
}

fn manhattan((x0, y0, z0): Point, (x1, y1, z1): Point) -> i32 {
    i32::abs(x1 - x0) + i32::abs(y1 - y0) + i32::abs(z1 - z0)
}

impl Bot {
    fn intersections(bots: impl IntoIterator<Item = Self>) -> BTreeMap<Self, BTreeSet<Self>> {
        let bots: Vec<Bot> = bots.into_iter().collect();
        let mut intersections = BTreeMap::<Bot, BTreeSet<Bot>>::new();
        for (i, a) in bots.iter().enumerate() {
            for b in &bots[i..] {
                if Bot::overlaps(a, b) {
                    intersections.entry(*a).or_default().insert(*b);
                    intersections.entry(*b).or_default().insert(*a);
                }
            }
        }
        intersections
    }

    fn in_radius(&self, other: &Self) -> bool {
        manhattan(self.pos, other.pos) <= self.r
    }

    fn overlaps(&self, other: &Self) -> bool {
        manhattan(self.pos, other.pos) <= self.r + other.r
    }
}

impl Into<BTreeSet<Point>> for Bot {
    fn into(self) -> BTreeSet<Point> {
        let Bot { pos: (x, y, z), r } = self;
        let mut set = BTreeSet::new();
        for dx in -r..=r {
            let x = x + dx;
            let slack = r - dx.abs();
            for dy in -slack..=slack {
                let y = y + dy;
                let slack = slack - dy.abs();
                for dz in -slack..=slack {
                    let z = z + dz;
                    {
                        set.insert((x, y, z));
                    }
                }
            }
        }
        set
    }
}
impl FromStr for Bot {
    type Err = ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut s = s.split(',').map(|s| {
            s.chars()
                .filter(|c| *c == '-' || c.is_ascii_digit())
                .collect::<String>()
        });
        if let (Some(x), Some(y), Some(z), Some(r)) = (s.next(), s.next(), s.next(), s.next()) {
            Ok(Bot {
                pos: (x.parse()?, y.parse()?, z.parse()?),
                r: r.parse()?,
            })
        } else {
            Err("aosidjaosd".parse::<u64>().unwrap_err())
        }
    }
}

impl Ord for Bot {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.r.cmp(&other.r).then_with(|| self.pos.cmp(&other.pos))
    }
}

impl PartialOrd for Bot {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}
#[test]
fn test_parse() {
    assert_eq!(
        "pos=<103063988,36317875,27455870>, r=92259913"
            .parse::<Bot>()
            .unwrap(),
        Bot {
            pos: (103063988, 36317875, 27455870),
            r: 92259913,
        }
    )
}
