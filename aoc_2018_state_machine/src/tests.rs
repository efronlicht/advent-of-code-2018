use super::*;
#[derive(Clone, PartialEq, Eq, Debug)]
struct Registers([u32; 4]);

impl std::ops::Index<u32> for Registers {
    type Output = u32;
    fn index(&self, i: u32) -> &u32 {
        &self.0[i as usize]
    }
}

impl std::ops::IndexMut<u32> for Registers {
    fn index_mut(&mut self, i: u32) -> &mut u32 {
        &mut self.0[i as usize]
    }
}
impl Register for Registers {}
#[test]
fn test_opcodes() {
    for (op, reg, (a, b, c), want) in vec![
        (OpCode::AddI, [1, 2, 3, 4], (3, 2, 3), 6), //a[3]+2 = 4 + 2 = 6
        (OpCode::AddR, [1, 2, 3, 4], (0, 0, 0), 2), // a[0]+a[0]= 1+1=2
        (OpCode::BAnI, [0b110, 0, 0, 0], (0, 0b011, 2), 2),
        (OpCode::BAnR, [0b110, 0b011, 0, 0], (0, 1, 2), 0b10),
        (OpCode::BOrR, [0b110, 0b011, 0, 0], (0, 1, 2), 0b111),
        (OpCode::BOrI, [0b10, 0, 0, 0], (0, 0b01, 2), 0b11),
        (OpCode::MulI, [2, 0, 0, 0], (0, 3, 2), 6),
        (OpCode::MulR, [2, 3, 0, 0], (0, 1, 2), 6),
        (OpCode::SetR, [0b10, 0, 0, 0], (0, 99999999, 2), 0b10),
        (OpCode::GtIR, [0, 1, 0, 0], (2, 1, 2), 1),
        (OpCode::GtRI, [5, 0, 0, 0], (0, 4, 2), 1),
        (OpCode::GtRR, [2, 1, 0, 0], (0, 1, 2), 1),
        (OpCode::SetI, [0, 0, 0, 0], (9999, 0, 0), 9999),
        (OpCode::EqIR, [0, 1, 0, 0], (1, 1, 2), 1),
        (OpCode::EqRI, [1, 0, 0, 0], (0, 1, 2), 1),
        (OpCode::EqRR, [1, 1, 0, 0], (0, 1, 2), 1),
    ] {
        let reg = Registers(reg);
        let got = reg.exec_copy(op, (a, b, c));

        assert_eq!(
            got[c], want,
            "{:?}({},{},{}) on {:?} => {:?}\n{:?}[{}] is {} but should be {:?}",
            op, a, b, c, reg, got, got, c, got[c], want
        );
    }
}
