fn main() {
    println!("aoc_2018_16_a => {}", a(TEST_INPUT));
    println!("aoc_2018_16_b => {}", b(TEST_INPUT, TEST_PROGRAM));
}
#[cfg(test)]
mod tests;
use regex::Regex;
use std::collections::{BTreeMap, BTreeSet};
use std::hash::Hash;
#[derive(Clone, PartialEq, Eq, Default, Debug)]
pub struct Registers([u32; 4]);

use std::ops::{Index, IndexMut};

fn flip<A, B>((a, b): (A, B)) -> (B, A) {
    (b, a)
}
fn reverse_mapping<K: Ord, V: Ord>(a: BTreeMap<K, V>) -> BTreeMap<V, K> {
    a.into_iter().map(flip).collect()
}

const TEST_INPUT: &str = include_str!("a.in");
const TEST_PROGRAM: &str = include_str!("test_program.in");
///Ignoring the opcode numbers, how many samples in your puzzle input behave like three or more opcodes?
fn a(input: &str) -> usize {
    let mut total = 0;
    for (before, after, (_, a, b, c)) in parse_input(input) {
        let mut matches = 0;
        for code in OpCode::iter() {
            if before.exec_copy(code, (a, b, c)) == after {
                matches += 1;
            }
            if matches >= 3 {
                total += 1;
                break;
            }
        }
    }
    total
}

/// Using the samples you collected, work out the number of each opcode and execute the test program (the second section of your puzzle input).
/// What value is contained in register 0 after executing the test program?
fn b(test_input: &str, test_program: &str) -> u32 {
    let opcode_numberings = find_opcode_numberings(test_input);
    let programs = parse_test_program(test_program).unwrap();
    let mut registers = Registers::default();

    for (opcode, a, b, c) in programs {
        let args = (a, b, c);
        let op = opcode_numberings[opcode as usize];
        registers.exec(op, args);
    }

    registers[0]
}

pub fn find_opcode_numberings(input: &str) -> [OpCode; 16] {
    let mut matches: BTreeMap<usize, BTreeMap<OpCode, usize>> = BTreeMap::new();
    for (before, after, (op_number, a, b, c)) in parse_input(input) {
        for code in OpCode::iter() {
            if before.exec_copy(code, (a, b, c)) == after {
                let matching_op = matches.entry(op_number as usize).or_default();
                *matching_op.entry(code).or_default() += 1;
            }
        }
    }
    let mut back: BTreeMap<OpCode, BTreeSet<usize>> = BTreeMap::new();

    for (i, m) in matches.iter() {
        for op in m.keys() {
            back.entry(*op).or_default().insert(*i);
        }
    }
    let mut found_opcodes = BTreeMap::<OpCode, usize>::new();
    println!("{:#?}", matches);

    fn find_unambigous_match(
        matches: &BTreeMap<usize, BTreeMap<OpCode, usize>>,
    ) -> Option<(usize, OpCode)> {
        if let Some((i, ops)) = matches.iter().find(|(_, ops)| ops.len() == 1) {
            let (op, _) = ops.iter().next().unwrap();
            Some((*i, *op))
        } else {
            None
        }
    }
    // remove all entries which match w/out question a specific opcode
    while let Some((i, op)) = find_unambigous_match(&matches) {
        dbg!((i, op));
        for j in &back[&op] {
            matches.get_mut(j).unwrap().remove(&op);
        }
        matches.remove(&i);
        found_opcodes.insert(op, i);
    }
    assert!(found_opcodes.len() == 16);

    dbg!(&found_opcodes);
    dbg!(matches);
    let mut out = [OpCode::AddI; 16];
    for (op, code) in found_opcodes {
        out[code] = op;
    }
    dbg!(&out);
    out
}

impl Index<u32> for Registers {
    type Output = u32;
    fn index(&self, i: u32) -> &u32 {
        self.0.index(i as usize)
    }
}

impl IndexMut<u32> for Registers {
    fn index_mut(&mut self, i: u32) -> &mut u32 {
        self.0.index_mut(i as usize)
    }
}
pub type Args = (u32, u32, u32);

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Input(OpCode, Args);

impl From<[u32; 4]> for Registers {
    fn from(a: [u32; 4]) -> Self {
        Registers(a)
    }
}

impl Into<[u32; 4]> for Registers {
    fn into(self) -> [u32; 4] {
        self.0
    }
}

fn is_match(before: [u32; 4], op: OpCode, args: Args, want: [u32; 4]) -> bool {
    let got = Registers::from(before).exec_copy(op, args);
    got.0 == want
}

impl Registers {
    pub fn exec_copy(&self, op: OpCode, (a, b, c): Args) -> Self {
        let mut copy = self.clone();
        copy[c] = copy.exec_binop(op, a, b);
        copy
    }
    pub fn exec(&mut self, op: OpCode, (a, b, c): Args) {
        self[c] = self.exec_binop(op, a, b)
    }

    fn exec_binop(&self, op: OpCode, a: u32, b: u32) -> u32 {
        let reg = self;
        match op {
            OpCode::AddI => reg[a] + b,
            OpCode::AddR => reg[a] + reg[b],

            OpCode::MulI => reg[a] * b,
            OpCode::MulR => reg[a] * reg[b],

            OpCode::BAnI => reg[a] & b,
            OpCode::BAnR => reg[a] & reg[b],

            OpCode::BOrI => reg[a] | b,
            OpCode::BOrR => reg[a] | reg[b],

            OpCode::SetI => a,
            OpCode::SetR => reg[a],

            OpCode::GtIR => {
                if a > reg[b] {
                    1
                } else {
                    0
                }
            }
            OpCode::GtRI => {
                if reg[a] > b {
                    1
                } else {
                    0
                }
            }
            OpCode::GtRR => {
                if reg[a] > reg[b] {
                    1
                } else {
                    0
                }
            }

            OpCode::EqIR => {
                if a == reg[b] {
                    1
                } else {
                    0
                }
            }
            OpCode::EqRI => {
                if reg[a] == b {
                    1
                } else {
                    0
                }
            }
            OpCode::EqRR => {
                if reg[a] == reg[b] {
                    1
                } else {
                    0
                }
            }
        }
    }
}

use self::OpCode::*;

impl OpCode {
    pub const OPCODES: [OpCode; 16] = [
        AddI, AddR, //
        MulI, MulR, //
        BAnI, BAnR, //
        BOrI, BOrR, //
        SetI, SetR, //
        GtIR, GtRI, GtRR, //
        EqIR, EqRI, EqRR, //
    ];

    pub fn iter() -> impl Iterator<Item = OpCode> {
        Self::OPCODES.iter().cloned()
    }
}

#[derive(Copy, Clone, PartialEq, PartialOrd, Ord, Eq, Debug)]
pub enum OpCode {
    /// addr (add register) stores into register C the result of adding register A and register B.
    AddR,
    /// addi (add immediate) stores into register C the result of adding register A and value B.
    AddI,
    /// mulr (multiply register) stores into register C the result of multiplying register A and register B.
    MulR,
    /// muli (multiply immediate) stores into register C the result of multiplying register A and value B.
    MulI,
    /// banr (bitwise AND reigster) stores into register C the result of the bitwise AND register A and register B.
    BAnR,
    /// bani (bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
    BAnI,
    /// borr (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
    BOrR,
    /// bori (bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
    BOrI,
    /// setr (set register) copies the contents of register A into register C. (Input B is ignored.)
    SetR,
    /// seti (set immediate) stores value A into register C. (Input B is ignored.)
    SetI,
    ///gtir (greater-than immediate/register) sets register C to 1 if value A is greater than register B. Otherwise, register C is set to 0.
    GtIR,
    ///gtri (greater-than register/immediate) sets register C to 1 if register A is greater than value B. Otherwise, register C is set to 0.
    GtRI,
    ///gtrr (greater-than register/register) sets register C to 1 if register A is greater than register B. Otherwise, register C is set to 0.
    GtRR,
    ///eqir (equal immediate/register) sets register C to 1 if value A is equal to register B. Otherwise, register C is set to 0
    EqIR,
    ///eqri (equal register/immediate) sets register C to 1 if register A is equal to value B. Otherwise, register C is set to 0.
    EqRI,
    ///eqrr (equal register/register) sets register C to 1 if register A is equal to register B. Otherwise, register C is set to 0.
    EqRR,
}

pub fn parse_input(input: &str) -> Vec<(Registers, Registers, (u32, u32, u32, u32))> {
    let state_re = Regex::new(r"\[(\d+), (\d+), (\d+), (\d+)\]").unwrap();
    let instructions_re = Regex::new(r"(\d+) (\d+) (\d+) (\d+)").unwrap();
    let mut out = Vec::new();
    let mut it = input.lines();
    while let (Some(before), Some(inputs), Some(after)) = (it.next(), it.next(), it.next()) {
        if let (Some(before), Some(after), Some(instructions)) = (
            state_re.captures(before),
            state_re.captures(after),
            instructions_re.captures(inputs),
        ) {
            let (before, after) = (
                Registers::from([
                    before[1].parse::<u32>().unwrap(),
                    before[2].parse().unwrap(),
                    before[3].parse().unwrap(),
                    before[4].parse().unwrap(),
                ]),
                Registers::from([
                    after[1].parse::<u32>().unwrap(),
                    after[2].parse().unwrap(),
                    after[3].parse().unwrap(),
                    after[4].parse().unwrap(),
                ]),
            );
            let instructions = (
                instructions[1].parse::<u32>().unwrap(),
                instructions[2].parse().unwrap(),
                instructions[3].parse().unwrap(),
                instructions[4].parse().unwrap(),
            );
            out.push((before, after, instructions));
            it.next(); // skip
        }
    }
    out
}

pub fn parse_test_program(s: &str) -> Result<Vec<(u32, u32, u32, u32)>, std::num::ParseIntError> {
    let mut out = Vec::new();
    for mut line in s
        .lines()
        .filter(|line| {
            !line.is_empty()
                || line.bytes().any(|b| match b {
                    b'0'..=b'9' => true,
                    _ => false,
                })
        })
        .map(|s| s.split_whitespace().map(|n| n.parse::<u32>()))
    {
        match (line.next(), line.next(), line.next(), line.next()) {
            (Some(op), Some(a), Some(b), Some(c)) => out.push((op?, a?, b?, c?)),
            _ => continue,
        }
    }
    Ok(out)
}
